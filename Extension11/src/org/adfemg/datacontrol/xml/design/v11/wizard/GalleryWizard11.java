package org.adfemg.datacontrol.xml.design.v11.wizard;

import oracle.ide.Context;

import oracle.jdeveloper.wizard.common.BaliWizardState;

import org.adfemg.datacontrol.xml.design.wizard.GalleryWizard;


public class GalleryWizard11 extends GalleryWizard {

    @Override
    protected BaliWizardState buildState(Context context) {
        return new GalleryWizardState11(context);
    }
}
