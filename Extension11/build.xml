<?xml version="1.0" encoding="UTF-8" ?>
<project xmlns="antlib:org.apache.tools.ant" name="Extension11" default="all" basedir=".">

  <property file="build.local.properties"/>
  <property file="build.properties"/>

  <!-- =====================================================
       target: -init
       ===================================================== -->
  <target name="-init">
    <tstamp>
      <!-- date and time in single integer results in too long number which JDev cannot process -->
      <format property="BUILDID" pattern="yyyyMMdd.HHmm" timezone="UTC"/>
    </tstamp>
    <echo>build id is ${BUILDID}</echo>
    <mkdir dir="${output.dir}"/>
    <mkdir dir="${deploy.dir}"/>
  </target>

  <!-- =====================================================
       target: all
       ===================================================== -->
  <target name="all" description="Build the project" depends="clean,javadoc,distsrc,jar,zip"/>

  <!-- =====================================================
       target: clean
       ===================================================== -->
  <target name="clean" description="Clean the project" depends="-init">
    <delete includeemptydirs="true">
      <fileset dir="${output.dir}" includes="**/*"/>
      <fileset dir="${deploy.dir}" includes="**/*"/>
    </delete>
    <echo>Cleaning ListOfValues project...</echo>
    <ant antfile="${lov.build.file}" target="clean" inheritall="false"/>
  </target>

  <!-- =====================================================
       target: jar
       ===================================================== -->
  <target name="jar" description="Deploy JDeveloper profiles" depends="-init">
    <taskdef name="ojdeploy" classname="oracle.jdeveloper.deploy.ant.OJDeployAntTask" uri="oraclelib:OJDeployAntTask"
             classpath="${oracle.home}/jdev/lib/ant-jdeveloper.jar"/>
    <!-- create absolute paths -->
    <property name="ojdeploy.abs"            location="${oracle.home}/jdev/bin/${ojdeploy.executable}"/>
    <property name="ojdeploy.jws.abs"        location="${ojdeploy.jws}"/>
    <property name="ojdeploy.output.abs"     location="${ojdeploy.output}"/>
    <property name="ojdeploy.script.abs"     location="${deploy.dir}/ojdeploy-build.xml"/>
    <property name="ojdeploy.log.abs"        location="${deploy.dir}/ojdeploy-statuslog.xml"/>
    <!-- run ojdeploy -->
    <ora:ojdeploy xmlns:ora="oraclelib:OJDeployAntTask" executable="${ojdeploy.abs}"
                  failonerror="true"
                  ora:buildscript="${ojdeploy.script.abs}"
                  ora:statuslog="${ojdeploy.log.abs}">
      <ora:deploy>
        <ora:parameter name="workspace"     value="${ojdeploy.jws.abs}"/>
        <ora:parameter name="project"       value="${ojdeploy.project}"/>
        <ora:parameter name="profile"       value="${ojdeploy.profile}"/>
        <ora:parameter name="failonwarning" value="true"/>
        <ora:parameter name="outputfile"    value="${ojdeploy.output.abs}"/>
      </ora:deploy>
    </ora:ojdeploy>
    <!-- append build id to version in extension.xml in jar -->
    <unzip src="${ojdeploy.output.abs}" dest="${deploy.dir}">
      <patternset includes="META-INF/extension.xml"/>
    </unzip>
    <sleep seconds="2"/> <!-- timestamp in ZIP has 2 seconds granularity. Wait long enough to ensure touched xml is new enough -->
    <replaceregexp file="${deploy.dir}/META-INF/extension.xml"
                   match="(&lt;extension[^&gt;]*\sversion=&quot;[^&quot;]*)(&quot;)"
                   replace="\1.${BUILDID}\2" flags="s"/>
    <zip update="true" destfile="${ojdeploy.output.abs}" basedir="${deploy.dir}" includes="META-INF/extension.xml"/>
    <!-- package LOV project -->
    <echo>Building ListOfValues project...</echo>
    <ant antfile="${lov.build.file}" target="jar" inheritall="false"/>
  </target>

  <!-- =====================================================
       target: zip
       ===================================================== -->
  <!-- dependency on jar target to ensure we use same version number in extension.xml and bundle.xml -->
  <target name="zip" description="create extension bundle ZIP for distribution" depends="-init,jar">
    <zip destfile="${deploy.dir}/${zip.base.name}-${BUILDID}.zip">
      <zipfileset prefix="META-INF" file="bundle.xml"/>
      <zipfileset dir="${deploy.dir}"     includes="${extension.id}.jar"/>
      <zipfileset dir="${lov.deploy.dir}" includes="*.jar"                prefix="${extension.id}/"/>
      <zipfileset dir="${deploy.dir}"     includes="${extension.id}*.zip" prefix="${extension.id}/" />
    </zip>
    <!-- append build id to version in bundle.xml in zip -->
    <unzip src="${deploy.dir}/${zip.base.name}-${BUILDID}.zip" dest="${deploy.dir}">
      <patternset includes="META-INF/bundle.xml"/>
    </unzip>
    <sleep seconds="2"/> <!-- timestamp in ZIP has 2 seconds granularity. Wait long enough to ensure touched xml is new enough -->
    <replaceregexp file="${deploy.dir}/META-INF/bundle.xml"
                   match="(&lt;u:version&gt;[^&lt;]*)(&lt;)"
                   replace="\1.${BUILDID}\2" flags="s"/>
    <zip update="true" destfile="${deploy.dir}/${zip.base.name}-${BUILDID}.zip" basedir="${deploy.dir}" includes="META-INF/bundle.xml"/>
  </target>

  <!-- =====================================================
       target: javadoc
       ===================================================== -->
  <target name="javadoc" description="create javadoc and package it in jar" depends="-init">
    <delete dir="${output.docdir}"/>
    <javadoc destdir="${output.docdir}" use="true" author="true" version="true" failonerror="true">
      <fileset dir="src"/>
      <fileset dir="../XMLDataControl/coresrc"/>
      <fileset dir="${lov.src.dir}"/>
      <fileset dir="${lov.core.src.dir}"/>
      <link href="http://docs.oracle.com/cd/E28280_01/apirefs.1111/e13403/"/> <!-- Extension SDK -->
      <link href="http://docs.oracle.com/cd/E28280_01/apirefs.1111/e10653/"/> <!-- ADF Model -->
      <link href="http://docs.oracle.com/cd/E28280_01/apirefs.1111/e10686/"/> <!-- ADF Share -->
      <classpath>
        <fileset dir="${oracle.home}/..">
          <include name="jdeveloper/ide/extensions/oracle.ide.jar"/>
          <include name="jdeveloper/ide/lib/javatools.jar"/>
          <include name="jdeveloper/ide/lib/oicons.jar"/>
          <include name="jdeveloper/ide/lib/uic.jar"/>
          <include name="jdeveloper/jdev/extensions/oracle.adf.share.dt.jar"/>
          <include name="jdeveloper/jdev/extensions/oracle.adfm.jar"/>
          <include name="jdeveloper/jdev/extensions/oracle.ide.xmlef.jar"/>
          <include name="jdeveloper/jdev/extensions/oracle.javacore.jar"/>
          <include name="jdeveloper/jdev/extensions/oracle.j2ee.webapp.jar"/>
          <include name="jdeveloper/jdev/lib/javacore.jar"/>
          <include name="jdeveloper/jdev/lib/xmleditor.jar"/>
          <include name="modules/javax.jsf*.jar"/>
          <include name="modules/javax.jsp*.jar"/>
          <include name="oracle_common/modules/com.bea.core.apache.commons.lang*.jar"/>
          <include name="oracle_common/modules/javax.servlet*.jar"/>
          <include name="oracle_common/modules/oracle.adf.model_*/adfdt_common.jar"/>
          <include name="oracle_common/modules/oracle.adf.model_*/adfm.jar"/>
          <include name="oracle_common/modules/oracle.adf.share.ca_*/adf-share-base.jar"/>
          <include name="oracle_common/modules/oracle.adf.share.ca_*/adf-share-ca.jar"/>
          <include name="oracle_common/modules/oracle.adf.view_*/adf-richclient-api*.jar"/>
          <include name="oracle_common/modules/oracle.adf.view_*/jewt4.jar"/>
          <include name="oracle_common/modules/oracle.adf.view_*/trinidad-api.jar"/>
          <include name="oracle_common/modules/oracle.xdk_*/xmlparserv2.jar"/>
        </fileset>
      </classpath>
    </javadoc>
    <zip filesonly="true" destfile="${deploy.dir}/${extension.id}-doc.zip">
      <fileset dir="${output.docdir}"/>
    </zip>
  </target>

  <!-- =====================================================
       target: distsrc
       ===================================================== -->
  <target name="distsrc" depends="-init"
          description="Create source zip">
    <zip filesonly="true" destfile="${deploy.dir}/${extension.id}-src.zip">
      <fileset dir="src" excludes="META-INF/**"/>
      <fileset dir="../XMLDataControl/coresrc"/>
      <fileset dir="${lov.src.dir}"/>
      <fileset dir="${lov.core.src.dir}"/>
    </zip>
  </target>

</project>
