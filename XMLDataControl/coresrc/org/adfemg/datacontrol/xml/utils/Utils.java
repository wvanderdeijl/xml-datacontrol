package org.adfemg.datacontrol.xml.utils;

import java.beans.Beans;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.adf.model.adapter.AdapterException;

import oracle.xml.parser.v2.XMLNode;

import org.adfemg.datacontrol.xml.design.ClassLoaderUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * Generic Util methods for the XML DataControl.
 * Maybe these methods will be refactored to more specific utility classes
 * in the near future.
 */
public class Utils {
    private static final String SYSPROP_HANDLER_PKGS = "java.protocol.handler.pkgs";
    private static final String MDS_HANDLER_PKG = "oracle.mds.net.protocol";

    /**
     * Private constructor for non-instantiatable class.
     */
    private Utils() {
    }

    /**
     * Create a new instance of the given Class and wrap exceptions in a {@link AdapterException}.
     *
     * @param <T> The Type of the object to instantiate.
     * @param cls The class of the object to instantiate.
     * @return The object of the type <code>T</code>.
     */
    public static <T> T newInstance(final Class<T> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            throw new AdapterException(e);
        } catch (IllegalAccessException e) {
            throw new AdapterException(e);
        }
    }

    /**
     * Get an instance of the given interface <code>interfaceClass</code>.
     * All methods will throw an <code>UnsupportedOperationException</code>.
     *
     * This is usefull to create a marker instance of a specific interface.
     *
     * @param <T> The interface-type of the to be created object.
     * @param interfaceClass A <code>Class</code> object from the type <code>&lt;T></code>
     * @return an empty object of the type <code>&lt;T></code>
     */
    public static <T> T emptyInterfaceInstance(final Class<T> interfaceClass) {
        return interfaceClass.cast(Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class<?>[] {
                                                          interfaceClass }, new InvocationHandler() {
            public Object invoke(Object proxy, Method method, Object[] args) {
                throw new UnsupportedOperationException();
            }
        }));
    }

    /**
     * Ensures that the oramds URLs can be retrieved by adding the
     * <code>oracle.mds.net.protocol</code> to the system property
     * <code>java.protocol.handler.pkgs</code> if this is not done yet.
     */
    public static void ensureMdsHandlers() {
        String handlerPkgs = System.getProperty(SYSPROP_HANDLER_PKGS);
        if (handlerPkgs == null) {
            System.setProperty(SYSPROP_HANDLER_PKGS, MDS_HANDLER_PKG);
        } else if (!handlerPkgs.contains(MDS_HANDLER_PKG)) {
            System.setProperty(SYSPROP_HANDLER_PKGS, handlerPkgs + "|" + MDS_HANDLER_PKG);
        }
    }

    /**
     * Tries to create an Element out of an Object.
     *
     * @param obj instance of {@link Element} or a String
     * @return <code>obj</code> if it is already an <code>Element</code> or
     *         the parsed version of <code>obj</code> in case of a String.
     */
    public static Element toElement(final Object obj) {
        if (obj == null || obj instanceof Element) {
            return (Element) obj;
        }

        if (obj instanceof String) {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = builder.parse(new InputSource(new StringReader((String) obj)));
                return doc.getDocumentElement();
            } catch (Exception e) {
                throw new AdapterException(e);
            }
        }

        throw new IllegalArgumentException("Could not convert object of type: " + obj.getClass().getName() +
                                           " to org.w3c.dom.Element");
    }

    /**
     * Create a Node out of an XMLNode.
     * This can be usefull when an oracle specific operation needs to be used.
     * Such as <code>print</code>, while the Node is not an XMLNode, which is the
     * case for example for <code>javax.xml.soap.SOAPElement</code>.
     *
     * @param node The Node.
     * @return <code>node</code> if this is already an XMLNode was, or
     *         a copy of <code>node</code> which garantees to be an XMLNode.
     */
    public static XMLNode toXMLNode(final Node node) {
        if (node instanceof XMLNode) {
            return (XMLNode) node;
        }
        DocumentBuilder db = getBuilder();
        Document doc = db.newDocument();
        doc.appendChild(doc.importNode(node, true));
        return (XMLNode) doc.getDocumentElement();
    }

    /**
     * Creates a human friendly String out of an XMLNode.
     * Uses the <code>XMLNode.print</code> method.
     * @param node The XMLNode.
     * @return Readable String of the given node including indentation.
     */
    public static String xmlNodeToString(final XMLNode node) {
        StringWriter sw = new StringWriter();
        try {
            node.print(sw);
        } catch (IOException e) {
            throw new AdapterException(e);
        }
        return sw.toString();
    }

    public static Document parse(final String text) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder builder;
        try {
            builder = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
        InputStream stream = null;
        try {
            stream = new ByteArrayInputStream(text.getBytes("UTF-8"));
            return builder.parse(stream);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    /**
     * Find the first child element of a parent with the given name and namespace.
     *
     * @param parent The element in which we search for the child.
     * @param childNamespaceURI The namespace of the child to find.
     * @param childTagName The name of the child to find.
     * @return The first found child element or <code>null</code> if none found.
     *
     * @see #findFirstChildElement(Element,String)
     * @see #findChildElements(Element,String,String)
     */
    public static Element findFirstChildElement(final Element parent, final String childNamespaceURI,
                                                final String childTagName) {
        NodeList children = parent.getChildNodes();
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (isElementNS(child, childNamespaceURI, childTagName)) {
                return (Element) child;
            }
        }
        return null;
    }

    /**
     * Find the first child element of a parent with the given name and with
     * the same namespace as the parent.
     *
     * @param parent The element in which we search for the child.
     * @param childTagName The name of the child to find.
     * @return The first found child element or <code>null</code> if none found.
     *
     * @see #findFirstChildElement(Element,String,String)
     */
    public static Element findFirstChildElement(final Element parent, final String childTagName) {
        return findFirstChildElement(parent, parent.getNamespaceURI(), childTagName);
    }

    /**
     * Finds all the child elements of a parent with the given name and namespace.
     *
     * @param parent The element in which we search for the child.
     * @param childNamespaceURI The namespace of the child to find.
     * @param childTagName The name of the child to find.
     * @return A list of the found child elements or an empty List, but never <code>null</code>.
     *
     * @see #findFirstChildElement(Element,String)
     * @see #findChildElements(Element,String,String)
     */
    public static List<Element> findChildElements(final Element parent, final String childNamespaceURI,
                                                  final String childTagName) {
        List<Element> retval = new ArrayList<Element>(5);
        NodeList children = parent.getChildNodes();
        for (int i = 0, n = children.getLength(); i < n; i++) {
            Node child = children.item(i);
            if (isElementNS(child, childNamespaceURI, childTagName)) {
                retval.add((Element) child);
            }
        }
        return retval;
    }

    /**
     * Determines if a node is a XML element and it has the correct namespace and tagName.
     * As a workaround for a Oracle JAX-WS bug this also considers a null namespace on the element to
     * be equal to the empty ("") namespace.
     * @param node node to test
     * @param namespaceURI namespace the node should have. when testing for empty namespace (""), this method
     *                     will also accept nodes with a null namespace to be equal.
     * @param tagName tagName the node should have
     * @return <code>true</code> if the supplied node is an element with the correct namespace and tagName.
     * otherwise <code>false</code>
     */
    private static boolean isElementNS(final Node node, final String namespaceURI, final String tagName) {
        return node instanceof Element &&
               (namespaceURI.equals(node.getNamespaceURI()) ||
                ("".equals(namespaceURI) && node.getNamespaceURI() == null)) && tagName.equals(node.getLocalName());
    }

    /**
     * Finds all the child elements of a parent with the given name and with
     * the same namespace as the parent.
     *
     * @param parent The element in which we search for the child.
     * @param childTagName The name of the child to find.
     * @return A list of the found child elements or an empty List, but never <code>null</code>.
     *
     * @see #findFirstChildElement(Element,String,String)
     */
    public static List<Element> findChildElements(final Element parent, final String childTagName) {
        return findChildElements(parent, parent.getNamespaceURI(), childTagName);
    }

    /**
     * Ask for an empty DocumentBuilder.
     * @return the empty DocumentBuilder.
     */
    public static DocumentBuilder getBuilder() {
        DocumentBuilderFactory dbf;
        DocumentBuilder builder;

        dbf = DocumentBuilderFactory.newInstance();
        try {
            builder = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new AdapterException(e);
        }
        return builder;
    }

    public static ClassLoader getWorkspaceClassLoader() {
        return Beans.isDesignTime() ? ClassLoaderUtil.getDesignTimeClassLoader() :
               Thread.currentThread().getContextClassLoader();
    }
}
