package org.adfemg.datacontrol.xml.data;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.namespace.QName;

import oracle.adf.model.adapter.dataformat.AttributeDef;
import oracle.adf.model.adapter.dataformat.StructureDef;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.AttributeDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.DataControlDefinition;
import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.LeafNodeType;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.handler.HandlerRegistry;
import org.adfemg.datacontrol.xml.handler.InsteadGetHandler;
import org.adfemg.datacontrol.xml.handler.InsteadPutHandler;
import org.adfemg.datacontrol.xml.handler.OperationHandler;
import org.adfemg.datacontrol.xml.handler.PostPutHandler;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.Utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Wrapper for a XML Element to present it self as defined by a {@link StructureDefinition}.
 */
public class XMLDCElement implements XMLDCAccessorTarget, Map<String, Object> {

    private static final ADFLogger logger = ADFLogger.createADFLogger(XMLDCElement.class);

    // constructor args
    private final DataControl dc;
    private final StructureDefinition structDef;
    private final Element element;

    // internal properties (can also be used by customizers)
    Map<String, Object> properties;

    // based on StructureDefinition
    private final LinkedHashMap<String, Attribute> attributes = new LinkedHashMap<String, Attribute>();
    private final LinkedHashMap<String, Accessor> accessors = new LinkedHashMap<String, Accessor>();
    private final HandlerRegistry handlers;

    //////////////////// Constructor ////////////////////

    /**
     * Constructor.
     * @param dc DataControl  Instance which this collection is part of.
     * @param structDef StructureDefinition that describes how this element should behave
     *            at runtime and what structure the DataControl has.
     * @param element XML Element that returns the runtime information for this DataControl.
     */
    public XMLDCElement(final DataControl dc, final StructureDefinition structDef, final Element element) {
        this.dc = dc;
        this.structDef = structDef;
        this.element = element;
        this.handlers = new HandlerRegistry((StructureDef) structDef);

        // wrap attributes and accessors with their own class for lazy resolving
        for (Iterator iter = structDef.getAttributeDefinitions().iterator(); iter.hasNext();) {
            Object obj = iter.next();
            if (obj instanceof AttributeDef) {
                AttributeDefinition attr = (AttributeDefinition) obj;
                attributes.put(attr.getName(), new Attribute(attr));
            }
        }
        for (Iterator iter = structDef.getAccessorDefinitions().iterator(); iter.hasNext();) {
            Object obj = iter.next();
            if (obj instanceof AccessorDefinition) {
                AccessorDefinition acc = (AccessorDefinition) obj;
                accessors.put(acc.getName(), new Accessor(acc));
            }
        }

        // call @Created
        handlers.invokeCreated(this);
    }

    //////////////////// java.util.Map implementation ////////////////////

    /**
     * Returns the amount of defined attributes and accessors in this element.
     * @return the amount of defined attributes and accessors in the
     *         StructureDefinition. These don't have to contain a value.
     */
    @Override
    public int size() {
        return attributes.size() + accessors.size();
    }

    /**
     * @return <code>true</code> when the StructureDefinition has no attribute or
     *         accessor, else will return <code>false</code>.
     */
    @Override
    public boolean isEmpty() {
        return attributes.isEmpty() && accessors.isEmpty();
    }

    /**
     * @param key Name of the attribute or accessor.
     * @return <code>true</code> if there is a definition for a attribute or
     *         accessor with this name, even if the value inside is <code>null</code>.
     * @see #containsAttribute
     * @see #containsAccessor
     */
    @Override
    public boolean containsKey(final Object key) {
        return attributes.containsKey(key) || accessors.containsKey(key);
    }

    /**
     * This method is not implemented because it would require the values of all the
     * attributes and accessors to be desided.
     * This has a relatively high cost, while expected is that the <code>containsValue</code>
     * isn't needed.
     * @param value nvt
     * @return Always throws an UnsupportedOperationException
     * @throws UnsupportedOperationException
     */
    @Override
    public boolean containsValue(final Object value) {
        throw new UnsupportedOperationException("XMLDCElement.containsValue is not supported");
    }

    /**
     * Gets the value of an attribute or accessor, or <code>null</code> if not found.
     *
     * @param key The name of the attribute or the accessor.
     * @return In case of an attribute, this will be the value of the java object
     * as specified by the mapping in the {@link org.adfemg.datacontrol.xml.provider.typemap.TypeMapper}.
     * In case of an accessor, the {@link XMLDCAccessorTarget} to which
     * this accessor points. For accessors pointing to a collection this will be
     * a {@link XMLDCCollection}, while non-collection accesors will return a
     * {@link XMLDCElement}.
     */
    @Override
    public Object get(final Object key) {
        if (attributes.containsKey(key)) {
            Attribute attribute = attributes.get(key);
            String attrName = attribute.getDefinition().getName();
            if (handlers.hasHandler(InsteadGetHandler.class, attrName)) {
                return handlers.invokeInsteadGet(this, attrName);
            }
            if (attribute.javaValue == null) {
                Node node = resolveAttribute(attribute);
                if (node != null) {
                    attribute.javaValue =
                        getTypeMapper().toJava(node.getTextContent(), attribute.getXmlType(),
                                               attribute.def.getJavaTypeString());
                }
            }
            return attribute.javaValue;
        } else if (accessors.containsKey(key)) {
            return resolveAccessor(accessors.get(key));
        } else {
            logger.warning("Unknown attribute or accessor: {0}", key);
            return null;
        }
    }

    /**
     * Changes the value of an attribute within this element.
     * We use the {@link org.adfemg.datacontrol.xml.provider.typemap.TypeMapper}.
     *
     * @param key The name of the attribute to put.
     * @param value The new value to put on the attribute.
     * @return The old value of the attribute, this can be <code>null</code>.
     * @throws IllegalArgumentException If the key is not the name of an attribute.
     */
    @Override
    public Object put(final String key, final Object value) {
        if (attributes.containsKey(key)) {
            // fire PrePut handlers (if any)
            final AttrChangeEvent attrChangeEvent = new AttrChangeEvent(get(key), value, key, this);
            handlers.invokePrePut(attrChangeEvent);

            // perform actual XML manipulation (or invoke InsteadPutHandler if present)
            Object oldVal = putInternal(key, value);

            // fire PostPut handlers (if any)
            if (handlers.hasHandler(PostPutHandler.class, key)) {
                handlers.invokePostPut(attrChangeEvent, key);
            }

            return oldVal;
        } else if (accessors.containsKey(key)) {
            Accessor accessor = accessors.get(key);
            if (accessor.def.isCollection()) { // An accessor to an collection will always contain a (possible empty) collection.
                throw new IllegalArgumentException("kan via put geen collectie-accessor maken");
            }
            XMLDCElement oldTarget = (XMLDCElement) get(key); // Can not be an collection.
            if (oldTarget != null) { // Found existing element.
                if (value != null) {
                    throw new IllegalArgumentException("Can not override the accessor through put, only remove.");
                } else { // Remove an existing element.
                    Element removeElem = oldTarget.element;
                    removeElem.getParentNode().removeChild(removeElem);
                    accessor.reset();
                    return oldTarget;
                }
            }
            throw new IllegalArgumentException("Can not create an accessor through put.");
        } else {
            throw new IllegalArgumentException("\"" + key + "\" is not an attribute or accessor in " + this);
        }
    }

    /**
     * Changes attribute value without calling any PrePut or PostPut handlers
     * thereby not giving handlers an opportunity to mark/detect the attribute
     * as changed. Is invoked from {@link #put} but can also be invoked by
     * clients when they want to set a value without triggering normal pre- and
     * post-processing (for example setting a default value during object
     * creation)
     * @param key the name of the attribute.
     * @param value the new value of the attribute.
     * @return the old value, this can be <code>null</code>.
     */
    public Object putInternal(final String key, final Object value) {
        if (!attributes.containsKey(key)) {
            throw new IllegalArgumentException("attribute not found " + key);
        }
        Attribute attribute = attributes.get(key);
        Object oldVal = get(key);

        // fire InsteadPutHandler (if present) or perform normal processing
        if (handlers.hasHandler(InsteadPutHandler.class, key)) {
            final AttrChangeEvent attrChangeEvent = new AttrChangeEvent(oldVal, value, key, this);
            handlers.invokeInsteadPut(attrChangeEvent, key);
        } else {
            String newXmlVal = getTypeMapper().toXml(value, attribute.getXmlType());
            Node node = resolveAttribute(attribute);
            attribute.reset(); // reset so next fetch will re-resolve
            // TODO: not sure if this works with a XMLElement with attributes AND SimpleContent
            // this is a special case as we cannot remove the element when clearing the SimpleContent when other
            // attributes exists.
            // Also setting or clearing the special "_value" attribute shouldn't end up as <_value> in XML
            if (node != null) { // Found existing node to edit/remove.
                if (newXmlVal == null) { // Set value to null, so remove node from XML.
                    removeNode(attribute, node);
                } else {
                    node.setTextContent(newXmlVal);
                }
            } else { // No node exist, so we create one.
                if (newXmlVal != null) {
                    if (LeafNodeType.ATTRIBUTE.equals(attribute.getLeafNodeType())) {
                        element.setAttributeNS(attribute.getXmlNamespaceUri(), attribute.getXmlName(), newXmlVal);
                    } else if (LeafNodeType.ELEMENT.equals(attribute.getLeafNodeType())) {
                        // nieuw element maken
                        createNewElement(attribute, newXmlVal);
                    } else {
                        throw new IllegalStateException("unsupported leaf node type: " + attribute.getLeafNodeType());
                    }
                }
            }
        }
        return oldVal;
    }

    /**
     * Creating a new Element for the attribute and its new value.
     * We create a new element with the correct name & namespace and set the
     * new value based on the input parameter.
     *
     * We look for the nextSibling Node and insert the Element before this Sibling.
     *
     * @param attribute the attribute it concerns.
     * @param newXmlVal the new value the attribute gets.
     */
    private void createNewElement(Attribute attribute, String newXmlVal) {
        Element newElem =
            element.getOwnerDocument().createElementNS(attribute.getXmlNamespaceUri(), attribute.getXmlName());
        newElem.setTextContent(newXmlVal);
        // Find the next (non empty) child element within parent
        boolean foundInsertingElem = false;
        Node nextSibling = null;
        for (Attribute otherAttr : attributes.values()) {
            if (foundInsertingElem && LeafNodeType.ELEMENT.equals(otherAttr.getLeafNodeType()) &&
                resolveAttribute(otherAttr) != null) { // Found next filled element
                nextSibling = otherAttr.node;
                break;
            }
            foundInsertingElem = foundInsertingElem || (otherAttr == attribute);
        }
        element.insertBefore(newElem, nextSibling); // if nextSibling==null will be added at the end.
    }

    /**
     * Private method to remove a Node from the XMLDCElement.
     * Node will be removed from the parent Node and
     * the attribute node will be set to the emptyInterfaceInstance.
     *
     * @param attribute the attribute it concerns.
     * @param node the node to remove.
     */
    private void removeNode(Attribute attribute, Node node) {
        Node parent = node.getParentNode();
        parent.removeChild(node);
        attribute.node = XMLDCElement.Attribute.NOT_FOUND;
    }

    /**
     * Changes the value of an attribute or accessor within the element to <code>null</code>.
     *
     * @param key Name of the attribute or accessor to remove.
     * @return the previous/old value of the attribute or accessor.
     */
    @Override
    public Object remove(final Object key) {
        throw new UnsupportedOperationException();
    }

    /**
     * Copies all the values from the given map to attributes or accessors of this map
     * by calling the {@link #put} for every entry in the given map.
     *
     * @param map the mappings that need to be copied to the map.
     */
    @Override
    public void putAll(final Map<? extends String, ? extends Object> map) {
        for (Map.Entry<? extends String, ? extends Object> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Not implemented.
     * @throws UnsupportedOperationException
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException("XMLDCElement.clear not implemented");
    }

    /**
     * Gets a {@link Set} off all the attribute and accessor keys in this element definition.
     *
     * @return A unmodifiableSet of the names of attributes and accessors.
     */
    @Override
    public Set<String> keySet() {
        Set<String> keys = new HashSet<String>(attributes.keySet());
        keys.addAll(accessors.keySet());
        return Collections.unmodifiableSet(keys);
    }

    /**
     * Not implemented because this would require all the values of all the attributes
     * and all accessors to be determined, this has a relatively high cost, while you
     * expect that this is not needed.
     *
     * @return Always throws an UnsupportedOperationException
     * @throws UnsupportedOperationException
     */
    @Override
    public Collection<Object> values() {
        throw new UnsupportedOperationException("XMLDCElement.values is not supported. ");
    }

    /**
     * Not implemented because this would require all the values of all the attributes
     * and all accessors to be determined, this has a relatively high cost, while you
     * expect that this is not needed.
     *
     * @return Always throws an UnsupportedOperationException
     * @throws UnsupportedOperationException
     */
    @Override
    public Set<Map.Entry<String, Object>> entrySet() {
        throw new UnsupportedOperationException("XMLDCElement.entrySet is not supported. ");
    }

    //////////////////// geef inzage of een attribuut of accessor bestaat ////////////////////

    /**
     * Returns <code>true</code> if the StructureDefinition has an attribute with
     * the given name.
     *
     * @param key The name of the attribute.
     * @return Returns <code>true</code> if the StructureDefinition has an attribute with
     *                the given name, this can be the value <code>null</code>.
     * @see #containsKey
     */
    public boolean containsAttribute(final Object key) {
        return attributes.containsKey(key);
    }

    /**
     * Returns <code>true</code> if the StructureDefinition has an accessor with
     * the given name.
     * @param key The name of the accessor.
     * @return Returns <code>true</code> if the StructureDefinition has an accessor with
     *                the given name, this can be the value <code>null</code>.
     * @see #containsKey
     */
    public boolean containsAccessor(final Object key) {
        return accessors.containsKey(key);
    }

    protected final Accessor getAccessor(final Object name) {
        return accessors.get(name);
    }

    public boolean isCollection(final Object accessorName) {
        Accessor acc = accessors.get(accessorName);
        return (acc != null && acc.def.isCollection());
    }

    //////////////////// lazy resolving of attribute or accessor ////////////////////

    /**
     * Gets the node for a given attribute.
     * The response will be put into the <code>attribute</code> itself, so only the
     * first time we determine the XML Node and after that we can return the same Node.
     *
     * @param attribute Attribute from the XMLDCElement that contains an AttributeDef.
     * @return XML Element or Attribute XML node the given <code>Attribute</code>
     *         points to or <code>null</code> if there is no such node.
     */
    protected Node resolveAttribute(final Attribute attribute) {
        if (attribute.node == null) { // attribute.node doesn't exist so determine it.
            if (LeafNodeType.SCALAR_COLLECTION_ELEMENT.equals(attribute.getLeafNodeType())) { // This is an element withe a SimpleType in a collection.
                // In this case there is a "fake" attribute to get the value.
                attribute.node = element;
            } else {
                attribute.node =
                    LeafNodeType.ATTRIBUTE.equals(attribute.getLeafNodeType()) ?
                    element.getAttributeNodeNS(attribute.getXmlNamespaceUri(), attribute.getXmlName()) :
                    Utils.findFirstChildElement(element, attribute.getXmlNamespaceUri(), attribute.getXmlName());
                if (attribute.node == null) {
                    attribute.node = XMLDCElement.Attribute.NOT_FOUND;
                }
            }
        }
        return attribute.node == XMLDCElement.Attribute.NOT_FOUND ? null : attribute.node;
    }

    /**
     * Gets or creates the XMLDCAccessorTarget for a given accessor.
     * The response will be put into the <code>accessor</code> itself, so only the
     * first time we determine the XMLDCAccessorTarget and after that we can
     * return the same XMLDCAccessorTarget.
     *
     * @param accessor Accessor from the XMLDCElement that contains an AccessorDef.
     * @return In case of an accossor to a collection, a (possible empty) XMLDCCollection
     *         In case of a singular an instance of the XMLDCElement if the child
     *         object exist, if the child object doesn't exist <code>null</code>.
     */
    protected XMLDCAccessorTarget resolveAccessor(final Accessor accessor) {
        if (accessor.target == null) {
            AccessorDefinition accessorDef = accessor.def;
            if (accessorDef.isCollection()) { // accessor is a collection
                accessor.target = new XMLDCCollection(dc, accessorDef, element);
            } else { // accessor to a singular object
                Node childNode =
                    Utils.findFirstChildElement(element, accessor.getXmlNamespaceUri(), accessor.getXmlName());
                if (childNode != null) {
                    accessor.target = new XMLDCElement(dc, accessorDef.getStructure(), (Element) childNode);
                } else {
                    accessor.target = XMLDCElement.Accessor.NOT_FOUND;
                }
            }
        }
        return accessor.target == XMLDCElement.Accessor.NOT_FOUND ? null : accessor.target;
    }

    public XMLDCAccessorTarget createChild(final String accessorName) {
        Accessor accessor = accessors.get(accessorName);
        if (accessor == null) {
            throw new IllegalArgumentException("accessor \"" + accessorName + "\" does not exist in " + this);
        }
        Element newElem =
            element.getOwnerDocument().createElementNS(accessor.getXmlNamespaceUri(), accessor.getXmlName());
        Node nextSibling =
            findInsertBeforeSibling(accessorName); // Find the first (non empty) child element within the parent
        element.insertBefore(newElem, nextSibling); // if nextSibling==null we add it to the end.
        accessor.reset();
        return resolveAccessor(accessor);
    }

    /**
     * Find the child node that should be used on element.insertBefore
     * @param insertingAccessorName name of the accessor (child) we want to insert
     * @return first sibling after the position where we need to add the child
     *         or null if none was found and the new child should be inserted as last child
     */
    public Node findInsertBeforeSibling(final String insertingAccessorName) {
        Accessor insAcc = accessors.get(insertingAccessorName);
        QName insName = new QName(insAcc.getXmlNamespaceUri(), insAcc.getXmlName());
        boolean pastInserting = false;

        // get ordered list of possible child elements so we know position where to add children
        List<QName> childNames =
            (List<QName>) getDefinition().getProperty(DataControlDefinition.STRUCTPROP_CHILD_ELEMS);
        if (childNames == null) {
            childNames = Collections.emptyList();
        }

        for (QName child : childNames) {
            if (!pastInserting) {
                pastInserting = (insName.equals(child));
                continue;
            }
            // Iteration is passed the element 'to add', so we look if this element exists
            Element elem = Utils.findFirstChildElement(element, child.getNamespaceURI(), child.getLocalPart());
            if (elem != null) { // child-element found, this is the insert-before-point
                return elem;
            }
        }
        return null; // nothing found, return null so the insert will be at the end.
    }

    @Override
    public String toString() {
        return new StringBuilder().append(getClass().getSimpleName()).append("[element:").append(element == null ?
                                                                                                 null :
                                                                                                 element.getNodeName()).append(",dc:").append(dc).append("]").toString();
    }

    public Element getElement() {
        return element;
    }

    //////////////////// method support ////////////////////

    public boolean hasMethod(String methodName, Map arguments) {
        return findOperation(methodName, arguments) != null;
    }

    public Object invokeMethod(String methodName, Map arguments) {
        return findOperation(methodName, arguments).invokeOperation(this, arguments);
    }

    private OperationHandler findOperation(String methodName, Map arguments) {
        for (OperationHandler operation : handlers.getHandlers(OperationHandler.class)) {
            if (operation.handlesOperation(methodName, arguments)) {
                return operation;
            }
        }
        return null;
    }

    public DataControl getDataControl() {
        return dc;
    }

    public StructureDefinition getDefinition() {
        return structDef;
    }

    /**
     * Returns a (mutable) map of implementation properties for this XMLDCElement instance.
     * Can be used for implementation specific details like change tracking, transient field
     * values, etc. It is up to the caller to ensure the keys that are used within this map
     * are unique among the different consumers that want to retain state in the properties.
     * @return Mutable Map, never <code>null</code>
     */
    public Map<String, Object> getProperties() {
        if (properties == null) {
            properties = new ConcurrentHashMap<String, Object>();
        }
        return properties;
    }

    protected TypeMapper getTypeMapper() {
        DataControlDefinition dcDef = getDataControl().getDCDefinition();
        DataControlDefinitionNode defNode = dcDef.findDefinitionNode(getDefinition());
        return defNode.getProviderInstance(TypeMapper.class);
    }

    /**
     * Class to wrap an AttributeDefinition that can be lazy resolved to a real XML node.
     */
    protected static class Attribute {
        // Special marker for not found attributes.
        // This is to destinct between null (non resolved) Nodes and not found (resolved) Nodes.
        static final Node NOT_FOUND = Utils.emptyInterfaceInstance(Node.class);

        final AttributeDefinition def;
        Node node; // XMLNode to get the text from. Lazy cached.
        Object javaValue;

        Attribute(final AttributeDefinition def) {
            this.def = def;
        }

        /**
         * Reset the reference to the underlying XML NOde and all the derived values.
         */
        void reset() {
            this.node = null;
            this.javaValue = null;
        }

        public AttributeDefinition getDefinition() {
            return def;
        }

        QName getXmlType() {
            return (QName) def.getProperty(DataControlDefinition.ATTRPROP_TYPE);
        }

        LeafNodeType getLeafNodeType() {
            return (LeafNodeType) def.getProperty(DataControlDefinition.ATTRPROP_LEAFNODETYPE);
        }

        String getXmlName() {
            return (String) def.getProperty(DataControlDefinition.ATTRPROP_NAME);
        }

        String getXmlNamespaceUri() {
            return (String) def.getProperty(DataControlDefinition.ATTRPROP_NAMESPACE);
        }
    }

    /**
     * Class to wrap an AccessorDefinition that can be lazy resolved to a XMLDCAccessorTarget.
     */
    protected static class Accessor {
        // Special marker for not found attributes.
        // This is to destinct between null (non resolved) Nodes and not found (resolved) Nodes.
        static final XMLDCAccessorTarget NOT_FOUND = Utils.emptyInterfaceInstance(XMLDCAccessorTarget.class);

        final AccessorDefinition def;
        XMLDCAccessorTarget target; // XMLDCCollection or XMLDCElement

        Accessor(final AccessorDefinition def) {
            this.def = def;
        }

        /**
         * Reset the reference to the underlying XML NOde and all the derived values.
         */
        void reset() {
            this.target = null;
        }

        public AccessorDefinition getDefinition() {
            return def;
        }

        String getXmlName() {
            return (String) def.getProperty(DataControlDefinition.ACCPROP_NAME);
        }

        String getXmlNamespaceUri() {
            return (String) def.getProperty(DataControlDefinition.ACCPROP_NAMESPACE);
        }

        int getMinOccurs() {
            Object obj = def.getProperty(DataControlDefinition.ACCPROP_MINOCCURS);
            return (obj instanceof Number ? ((Number) obj).intValue() : 1);
        }

        int getMaxOccurs() {
            Object obj = def.getProperty(DataControlDefinition.ACCPROP_MAXOCCURS);
            return (obj instanceof Number ? ((Number) obj).intValue() : 1);
        }

        boolean isNillable() {
            Object obj = def.getProperty(DataControlDefinition.ACCPROP_NILLABLE);
            return (obj instanceof Boolean ? ((Boolean) obj).booleanValue() : false);
        }
    }
}
