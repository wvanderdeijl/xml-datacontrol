package org.adfemg.datacontrol.xml.data;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;

import oracle.binding.meta.AccessorDefinition;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.DataControlDefinition;
import org.adfemg.datacontrol.xml.utils.Utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Wrapper for a plural (maxOccurs&gt;1) XML Element to behave as defined
 * by an {@link AccessorDefinition}.
 * This wrapper presents itself as a list of {@link XMLDCElement}.
 */
public class XMLDCCollection extends AbstractList<XMLDCElement> implements XMLDCAccessorTarget, RandomAccess {
    private final List<XMLDCElement> elements;
    private final DataControl dc;
    private final AccessorDefinition accessorDef;
    private final String childElemNamespace;
    private final String childElemName;
    private final Element parentElement;

    //////////////////// Constructor ////////////////////

    /**
     * Default constructor.
     * @param dc DataControl Instance which this collection is part of.
     * @param accessorDef AccessorDefinition Prescribes how the collection has to
     *                    behave at runtime and which names the XML Elements have.
     * @param parentElement The parent XML element, we can find the children with
     *        {@link Element#getChildNodes()} and put them on hte collection.
     */
    XMLDCCollection(final DataControl dc, final AccessorDefinition accessorDef, final Element parentElement) {
        this.dc = dc;
        this.accessorDef = accessorDef;
        this.parentElement = parentElement;
        this.childElemNamespace = (String) accessorDef.getProperty(DataControlDefinition.ACCPROP_NAMESPACE);
        this.childElemName = (String) accessorDef.getProperty(DataControlDefinition.ACCPROP_NAME);

        List<Element> children = Utils.findChildElements(parentElement, childElemNamespace, childElemName);
        this.elements = new ArrayList<XMLDCElement>(children.size());
        for (Element el : children) {
            this.elements.add(new XMLDCElement(dc, accessorDef.getStructure(), el));
        }
    }

    //////////////////// java.util.AbstractList implementation ////////////////////

    /**
     * Get the XMLDCElement on the asked position.
     * @param index index of the element that needs to be returned.
     * @return XMLDCElement the element.
     * @throws IndexOutOfBoundsException Will be thrown for an invalid index.
     */
    @Override
    public XMLDCElement get(final int index) {
        return elements.get(index);
    }

    /**
     * Returns the amount of elements in this collection.
     * @return the amount of elements in this collection.
     */
    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public XMLDCElement remove(final int index) {
        XMLDCElement elemDc = elements.get(index);
        Element xmlElem = elemDc.getElement();
        xmlElem.getParentNode().removeChild(xmlElem);
        // Everything went ok, we can safely remove the element from the internal list.
        return elements.remove(index);
    }

    /**
     *
     * @param index The index where the new element should be placed in front of.
     *              Use -1 for adding the first element to a empty collection.
     * @return the XMLDCElement.
     */
    public XMLDCElement createElement(final int index, final XMLDCElement master) {
        Element xmlElem = parentElement.getOwnerDocument().createElementNS(childElemNamespace, childElemName);
        XMLDCElement elemDc = new XMLDCElement(dc, accessorDef.getStructure(), xmlElem);
        if (index >= 0 && index < size()) { // toevoegen aan bestaande (niet lege) collectie
            Node nextSibling = get(index).getElement();
            parentElement.insertBefore(xmlElem, nextSibling); // nextSibling==null voegt aan einde toe
            elements.add(index, elemDc);
        } else { // zoek eerstvolgende sibling binnen parent
            Node nextSibling = master.findInsertBeforeSibling(accessorDef.getName());
            parentElement.insertBefore(xmlElem, nextSibling); // nextSibling==null voegt aan einde toe
            elements.add(elemDc);
        }
        return elemDc;
    }

}
