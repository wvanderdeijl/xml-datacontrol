package org.adfemg.datacontrol.xml.data;

/**
 * Object that can be refered in an accessor in an
 * {@link XMLDCElement}.
 */
public interface XMLDCAccessorTarget {
}
