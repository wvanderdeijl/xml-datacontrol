package org.adfemg.datacontrol.xml.cust;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.adfemg.datacontrol.xml.annotation.AnnotationHelper;

import oracle.adf.model.adapter.dataformat.AttributeDef;
import oracle.adf.model.adapter.dataformat.StructureDef;
import oracle.adf.share.logging.ADFLogger;


/**
 * Utility Class for the Adopter classes that implement de Adopter.
 *
 * @param <T> needs to be of the type Annotation.
 * @see Annotation
 * @see Adopter
 */
public abstract class AdopterUtil<T extends Annotation> implements Adopter<T> {
    private static final ADFLogger logger = ADFLogger.createADFLogger(AdopterUtil.class);

    protected static final boolean READONLY_TRUE = true;
    protected static final boolean READONLY_FALSE = false;
    protected static final boolean KEY_FALSE = false;

    private AdopterUtil() {
        super();
    }

    /**
     * Generic method to check a method signature.
     * Checks if:
     *    - The returnType is correct (assignable).
     *    - The amount of arguments are correct.
     *    - The arguments are from the same (assignable) Types.
     *
     * Throws an InvalidMethodSignatureException if the signature is incorrect.
     *
     * @param method The method off which the signature needs to be checked.
     * @param returnType The desired return type this method should have.
     * @param argTypes The desired arguments, zero or more argument types.
     * @throws InvalidMethodSignatureException
     * @see InvalidMethodSignatureException
     */
    protected static void checkSignature(final Method method, final Class returnType,
                                         Class... argTypes) throws InvalidMethodSignatureException {
        Class methodReturn = method.getReturnType();
        //FIXME: When to check on isPrimitive?! Based on a new argument?!
        if (!returnType.isAssignableFrom(methodReturn) && !methodReturn.isPrimitive()) {
            throw new InvalidMethodSignatureException("method return " + methodReturn + " for " + method +
                                                      " is not a " + returnType);
        }
        Class<?>[] methodParams = method.getParameterTypes();
        if (argTypes.length != methodParams.length) {
            throw new InvalidMethodSignatureException("invalid number of arguments for method " + method);
        }
        for (int i = 0; i < argTypes.length; i++) {
            if (!methodParams[i].isAssignableFrom(argTypes[i])) {
                throw new InvalidMethodSignatureException("method argument " + i + " for " + method + " is not a " +
                                                          argTypes[i]);
            }
        }
    }

    // FIXME: hoort eigenlijk niet in AdopterImpl. Deze is pas nodig tijdens
    // het uitvoeren van de customizations en dan verwacht je geen dependency
    // meer naar de adopter. Die zou klaar moeten zijn na initializatie van de
    // datacontrol

    protected static Object invokeMethod(Object obj, Method method, Object... args) {
        try {
            return method.invoke(obj, args);
        } catch (IllegalAccessException e) {
            logger.severe(e);
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
            }
            logger.severe(e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Utility method to get the attribute name based on the prefix or the suffix
     * of the method.
     *
     * @param method the java method to get the attribute from.
     * @param prefix the pre or suffix to strip from the method name.
     * @param isPrefix indicator wether it is a prefix or a suffix.
     * @return the attribute name.
     */
    protected static String getAttributeName(final Method method, final String prefix, final boolean isPrefix) {
        String methodName = method.getName();

        if (isPrefix) {
            if (!methodName.startsWith(prefix)) {
                throw new IllegalArgumentException("method name " + method.getName() + " should start with '" + prefix +
                                                   "'");
            }
            int prefixLength = prefix.length();

            return methodName.substring(prefixLength, prefixLength + 1).toLowerCase() +
                   methodName.substring(prefixLength + 1);
        } else {
            if (!methodName.endsWith(prefix)) {
                throw new IllegalArgumentException("method name " + methodName + " should end with '" + prefix + "'");
            }

            return methodName.substring(0, methodName.length() - prefix.length());
        }
    }

    protected static void checkMethodStartsWith(final Method method,
                                                final String prefix) throws InvalidMethodSignatureException {
        if (!method.getName().startsWith(prefix)) {
            throw new InvalidMethodSignatureException("Method name should begin with '" + prefix +
                                                      "', method name found: " + method.getName());
        }
    }

    protected static void checkMethodEndsWith(final Method method,
                                              final String suffix) throws InvalidMethodSignatureException {
        if (!method.getName().endsWith(suffix)) {
            throw new InvalidMethodSignatureException("Method name should end with '" + suffix +
                                                      "', method name found: " + method.getName());
        }
    }

    protected static void checkMethodEquals(final Method method,
                                            final String methodName) throws InvalidMethodSignatureException {
        if (!method.getName().equals(methodName)) {
            throw new InvalidMethodSignatureException("Method name should be '" + methodName +
                                                      "', method name found: " + method.getName());
        }
    }

    protected static void checkExistingAttributes(String[] attrs,
                                                  StructureDef structure) throws InvalidMethodSignatureException {
        if (!AnnotationHelper.isDefault(attrs)) {
            for (String attr : attrs) {
                if (structure.getAttributeDefinitions().find(attr) == null) {
                    throw new InvalidMethodSignatureException("Invalid attribute on annotation, attribute: " + attr +
                                                              " not foud.");
                }
            }
        }
    }

    /**
     * Utility class to add a attribute to the structure.
     *
     * @param structure The structure to add the new attribute to.
     * @param method The method, to get the returnType from.
     * @param attrName The name of the attribute.
     * @param isReadonly Indicates a readonly attribute.
     * @param isKey Indicates a key attribute.
     */
    protected static void addNewAttributeToStructure(StructureDef structure, Method method, final String attrName,
                                                     boolean isReadonly,
                                                     boolean isKey) {
        // TODO: Factory class die we kunnen delen met XSDProcessor
        final AttributeDef attrDef =
            new AttributeDef(attrName, structure, method.getReturnType().getName(), isReadonly, isKey);
        structure.addAttribute(attrDef);
    }
}
