package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.adfemg.datacontrol.xml.annotation.Operation;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.handler.OperationHandler;

import oracle.adf.model.adapter.dataformat.MethodDef;
import oracle.adf.model.adapter.dataformat.StructureDef;


/**
 * The Adporter for the Operation annotation.
 *
 * @see Adopter
 * @see Operation
 */
public class OperationAdopter implements Adopter<Operation> {
    /**
     * Check the method signature of the Operation.
     * We check if the first argument to the method is an XMLDCElement.
     *
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(Method method, Operation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        Class<?>[] args = method.getParameterTypes();
        if (args == null || args.length == 0 || !XMLDCElement.class.isAssignableFrom(args[0])) {
            throw new InvalidMethodSignatureException("first method argument for " + method +
                                                      " should be an XMLDCElement");
        }
    }

    /**
     * Adjusts the structure of the datacontrol and add a methodDef to it.
     *
     * We loop over the parameters of the method and add these to the new methodDef.
     * At the time being, we do not discover the real parameter name so we use the
     * name 'arg' concatenated with a number.
     *
     * If the returntype is void, we do not set a returntype.
     *
     * @see MethodDef
     * @inheritDoc
     */
    @Override
    public void adjustStructure(StructureDef structure, Operation annotation, final Method method) {
        final String methodName = method.getName();
        final MethodDef methodDef = new MethodDef(methodName, structure);
        int i = 0;
        for (Class param : method.getParameterTypes()) {
            if (i == 0) {
                // do not expose first (XMLDCElement) parameter in datacontrol structure
                i++;
                continue;
            }
            // FIXME: would be nice if we can discover the parameter names
            methodDef.addParameter(argName(i - 1), param.getName());
            i++;
        }
        Class returnType = method.getReturnType();
        if (!Void.TYPE.equals(returnType)) {
            methodDef.setReturnType(returnType.getName());
        }
        structure.addMethod(methodDef);
    }

    /**
     * Static method to create an argument name based a int.
     * We use the prefix 'arg' and concatenate this with the index.
     *
     * @param index the number of the argument.
     * @return the name of the argument.
     */
    static String argName(int index) {
        return "arg" + index;
    }

    /**
     * Creates a OperationHandler that fires the method created in the
     * customization class with the XMLDCElement as first arguments.
     *
     * @see OperationHandler
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public Collection<OperationHandler> createHandlers(final Operation annotation, final Method method,
                                                       final Object customizer) {
        OperationHandler handler = new OperationHandler() {
            /**
             * We add the XMLDCElement as first element to the List of arguments.
             * After that we add all the arguments from the map.
             */
            @Override
            public Object invokeOperation(XMLDCElement element, Map arguments) {
                List<Object> argVals = getArgumentValues(arguments);
                List<Object> allArgs = new ArrayList<Object>(argVals.size() + 1);
                allArgs.add(element); // first arg is always XMLDCElement
                allArgs.addAll(argVals); // add all other argument values
                Object retval = AdopterUtil.invokeMethod(customizer, method, allArgs.toArray());
                return retval;
            }

            @Override
            public boolean handlesOperation(String name, Map arguments) {
                boolean retval = method.getName().equals(name);
                Class<?>[] paramTypes = method.getParameterTypes();
                // we ignore first method argument as that is always XMLDCElement
                if (paramTypes.length != arguments.size() + 1) {
                    return false;
                }
                int i = 0;
                for (Object arg : getArgumentValues(arguments)) {
                    Class<?> paramType = paramTypes[i + 1];
                    if (paramType.isPrimitive()) {
                        // FIXME: check if arg can be unboxed to required primitive type
                        // for now we just assume it is
                        // retval = retval && (paramType.);
                    } else {
                        retval = retval && (paramType.isInstance(arg));
                    }
                    i++;
                }
                return retval;
            }

            private List<Object> getArgumentValues(Map args) {
                // return sorted list of argument values
                int size = args.size();
                List<Object> retval = new ArrayList<Object>(size);
                for (int i = 0; i < size; i++) {
                    retval.add(args.get(argName(i)));
                }
                return retval;
            }

        };

        return Collections.singletonList(handler);
    }
}
