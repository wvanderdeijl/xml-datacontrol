package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import org.adfemg.datacontrol.xml.annotation.CalculatedAttr;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.handler.InsteadGetHandler;

import oracle.adf.model.adapter.dataformat.StructureDef;


/**
 * The Adporter for the CalculatedAttr annotation.
 *
 * @see Adopter
 * @see CalculatedAttr
 */
public class CalculatedAttrAdopter implements Adopter<CalculatedAttr> {
    /**
     * Check the method signature of the CalculatedAttr method.
     * The method should start with 'get' followed with the attribute name.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(Method method, CalculatedAttr annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Object.class, XMLDCElement.class);
        AdopterUtil.checkMethodStartsWith(method, "get");
    }

    /**
     * Method to adjust the structure of the DataControl.
     * We add the calculated attribute as read only attribute to the current structure.
     * The name of the attribute is extracted from the method name in the Customization Class.
     *
     * @inheritDoc
     */
    @Override
    public void adjustStructure(StructureDef structure, CalculatedAttr annotation,
                                Method method) {
        // attribuut toevoegen aan StructureDef
        AdopterUtil.addNewAttributeToStructure(structure, method, getAttributeName(method), AdopterUtil.READONLY_TRUE,
                                               AdopterUtil.KEY_FALSE);
    }

    /**
     * Creates a InsteadGetHandler that fires the method created in the
     * customization class with the XMLDCElement as arguments.
     *
     * @see InsteadGetHandler
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public Collection<InsteadGetHandler> createHandlers(CalculatedAttr annotation, final Method method,
                                                        final Object customizer) {
        final String attrName = getAttributeName(method);
        InsteadGetHandler handler = new InsteadGetHandler() {
            @Override
            public Object doInsteadGet(XMLDCElement element) {
                final Object retval = AdopterUtil.invokeMethod(customizer, method, element);
                return retval;
            }

            @Override
            public boolean handlesAttribute(String input) {
                return attrName.equals(input);
            }
        };
        return Collections.singletonList(handler);
    }

    /**
     * Utility method to get the Attribute name from the method signature.
     *
     * @param method the method to get the attribute name from.
     * @return the attribute name.
     */
    private String getAttributeName(Method method) {
        return AdopterUtil.getAttributeName(method, "get", true);
    }
}
