package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import org.adfemg.datacontrol.xml.annotation.Created;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.handler.CreatedHandler;

import oracle.adf.model.adapter.dataformat.StructureDef;


/**
 * The Adporter for the Created annotation.
 *
 * @see Adopter
 * @see Created
 */
public class CreatedAdopter implements Adopter<Created> {
    /**
     * Check the method signature of the Created method.
     * The method should be equals to 'elementCreated'.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(Method method, Created annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, XMLDCElement.class);
        AdopterUtil.checkMethodEquals(method, "elementCreated");
    }

    @Override
    public void adjustStructure(StructureDef structure, Created annotation, Method method) {
        //No need to adjust the structure, the invoke needs to be fired on create.
        //Call is done at the end of the constructor in the XMLDCElement.

    }

    /**
     * Creates a CreatedHandler that fires the method created in the
     * customization class with the XMLDCElement as arguments.
     *
     * @see CreatedHandler
     * @see XMLDCElement
     * @inheritDoc
     */
    @Override
    public Collection<CreatedHandler> createHandlers(final Created annotation, final Method method,
                                                     final Object customizer) {
        CreatedHandler handler = new CreatedHandler() {
            @Override
            public void invokeCreated(XMLDCElement element) {
                AdopterUtil.invokeMethod(customizer, method, element);
            }
        };
        return Collections.singletonList(handler);
    }
}
