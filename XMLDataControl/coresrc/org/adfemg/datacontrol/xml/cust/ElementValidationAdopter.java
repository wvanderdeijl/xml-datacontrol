package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.annotation.AnnotationHelper;
import org.adfemg.datacontrol.xml.annotation.ElementValidation;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.events.ValidationListener;
import org.adfemg.datacontrol.xml.handler.PostPutHandler;

import oracle.adf.model.adapter.dataformat.StructureDef;


/**
 * The Adporter for the ElementValidation annotation.
 *
 * @see Adopter
 * @see ElementValidation
 */
public class ElementValidationAdopter implements Adopter<ElementValidation> {
    /**
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(Method method, ElementValidation annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, XMLDCElement.class);
        AdopterUtil.checkExistingAttributes(annotation.attr(), structure);
    }

    @Override
    public void adjustStructure(StructureDef structure, ElementValidation annotation, Method method) {
        //No need to adjust the structure.

    }

    /**
     * Creates a PostPutHandler that adds a validation listener to the datacontrol.
     *
     * @see PostPutHandler
     * @inheritDoc
     */
    @Override
    public Collection<PostPutHandler> createHandlers(final ElementValidation annotation, final Method method,
                                                     final Object customizer) {
        PostPutHandler handler = new PostPutHandler() {
            @Override
            public boolean handlesAttribute(String attrName) {
                // handle this attribute if annotation had no attribute-names
                // specified (so validation should occur on any attribute change)
                // or when the attribute being changed is part of the list of
                // attribute names in the annotation
                if (AnnotationHelper.isDefault(annotation.attr())) {
                    return true;
                }
                for (String annoAttr : annotation.attr()) {
                    if (attrName.equals(annoAttr)) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void doPostPut(AttrChangeEvent attrChangeEvent) {
                // add validation listener to invoke real validation when dataControl
                // initiates validation
                DataControl dc = attrChangeEvent.getElement().getDataControl();
                dc.addValidationListener(new ElementValidator(attrChangeEvent.getElement(), customizer, method));
            }

        };
        return Collections.singletonList(handler);
    }

    private static final class ElementValidator implements ValidationListener {
        private final XMLDCElement element;
        private final Object customizer;
        private final Method method;

        public ElementValidator(final XMLDCElement element, final Object customizer, final Method method) {
            this.element = element;
            this.customizer = customizer;
            this.method = method;
        }

        @Override
        public void validate() {
            AdopterUtil.invokeMethod(customizer, method, element);
        }
    }

}
