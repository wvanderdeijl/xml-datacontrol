package org.adfemg.datacontrol.xml.cust;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import oracle.adf.model.adapter.dataformat.MethodDef;
import oracle.adf.model.adapter.dataformat.MethodReturnDef;
import oracle.adf.model.adapter.dataformat.StructureDef;
import oracle.adf.share.logging.ADFLogger;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.DefinitionContainer;
import oracle.binding.meta.OperationDefinition;
import oracle.binding.meta.OperationReturnDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.annotation.AttrValidation;
import org.adfemg.datacontrol.xml.annotation.CalculatedAttr;
import org.adfemg.datacontrol.xml.annotation.Created;
import org.adfemg.datacontrol.xml.annotation.ElementCustomization;
import org.adfemg.datacontrol.xml.annotation.ElementValidation;
import org.adfemg.datacontrol.xml.annotation.Operation;
import org.adfemg.datacontrol.xml.annotation.PostAttrChange;
import org.adfemg.datacontrol.xml.annotation.TransientAttr;
import org.adfemg.datacontrol.xml.handler.Handler;
import org.adfemg.datacontrol.xml.handler.HandlerRegistry;
import org.adfemg.datacontrol.xml.provider.customization.CustomizationProvider;


/**
 * In this class we customize the datacontrol based on a customization class created
 * in the client project.
 */
public class Customizer {
    /**
     * private map to keep track of the annotations and their adopters.
     */
    private static final Map<Class<? extends Annotation>, Adopter> adopters =
        new HashMap<Class<? extends Annotation>, Adopter>() {
            {
                put(AttrValidation.class, new AttrValidationAdopter());
                put(CalculatedAttr.class, new CalculatedAttrAdopter());
                put(Created.class, new CreatedAdopter());
                put(ElementValidation.class, new ElementValidationAdopter());
                put(Operation.class, new OperationAdopter());
                put(PostAttrChange.class, new PostAttrChangeAdopter());
                put(TransientAttr.class, new TransientAttrAdopter());
            }
        };

    /**
     * customizers. key=target (datacontrol beanClass), value=collection of annotated customizer classes
     */
    private final Map<String, Collection<Object>> customizers;

    private Collection<Object> unusedCustomizers = Collections.EMPTY_LIST;

    private static final ADFLogger logger = ADFLogger.createADFLogger(Customizer.class);

    /**
     * The Constructor of the Customizer.
     * Initializes the Map of customizers based on the classes that are
     * provided in the DataContols.dcx.
     *
     * @param customizationProv the classes with the annotation ElementCustomization.
     */
    public Customizer(final CustomizationProvider customizationProv) {
        super();
        final Map<String, Collection<Object>> customizers = new HashMap<String, Collection<Object>>();
        Collection<? extends Object> providedCusts = customizationProv.getCustomizers();
        if (providedCusts != null) {
            for (Object customizer : providedCusts) {
                final ElementCustomization annotation = customizer.getClass().getAnnotation(ElementCustomization.class);
                if (annotation == null) {
                    throw new IllegalStateException("Customizer " + customizer.getClass().getName() + " doesn't have " +
                                                    ElementCustomization.class.getName() + " annotation");
                }
                final String target = annotation.target();
                if (!customizers.containsKey(target)) {
                    customizers.put(target, new ArrayList<Object>());
                }
                customizers.get(target).add(customizer);
            }
            for (String key : customizers.keySet()) {
                customizers.put(key, Collections.unmodifiableCollection(customizers.get(key)));
            }
        }
        this.customizers = Collections.unmodifiableMap(customizers);
        unusedCustomizers = new ArrayList<Object>(customizers.size());
        for (Collection<Object> custs : customizers.values()) {
            for (Object cust : custs) {
                unusedCustomizers.add(cust);
            }
        }
    }

    /**
     * Recursivly process all the posible customization on the structures that are reachable
     * through the accessors on the given structure.
     * @param accessors Accessors within a StructurDef.
     * @see StructureDef#getAccessorDefinitions
     */
    private void customizeAccessors(final DefinitionContainer accessors) {
        for (Iterator iterator = accessors.iterator(); iterator.hasNext();) {
            AccessorDefinition accessor = (AccessorDefinition) iterator.next();
            StructureDefinition struct = accessor.getStructure();
            if (struct instanceof StructureDef) {
                customize((StructureDef) struct);
            }
        }
    }

    /**
     * Process all posible customizations on the strucutres that are used as
     * method returns in the given structure.
     * @param operations Operatings within a StructureDef
     * @see StructureDef#getOperationDefinitions
     */
    private void customizeMethodReturns(final DefinitionContainer operations) {
        for (Iterator iterator = operations.iterator(); iterator.hasNext();) {
            OperationDefinition operation = (OperationDefinition) iterator.next();
            OperationReturnDefinition returnDef = operation.getOperationReturnType();
            if (returnDef instanceof MethodReturnDef) {
                MethodReturnDef mrd = (MethodReturnDef) returnDef;
                StructureDefinition definition = mrd.getStructure();
                if (definition instanceof StructureDef) {
                    customize((StructureDef) definition);
                }

            }
        }
    }

    /**
     * Entry point to the customizer class. Applies all customizations to the supplied MethodDef
     * and all its descendants.
     * @param method
     */
    public void customize(MethodDef method) {
        OperationReturnDefinition operReturn = method.getOperationReturnType();
        if (!(operReturn instanceof MethodReturnDef)) {
            return;
        }
        final StructureDefinition structDef = ((MethodReturnDef) operReturn).getStructure();
        if (structDef instanceof StructureDef) {
            customize((StructureDef) structDef);
            // warn for any customizer that wasn't used
            for (Object cust : unusedCustomizers) {
                logger.warning("unused customizer class {0} with target {1}", new Object[] {
                               cust.getClass(), cust.getClass().getAnnotation(ElementCustomization.class).target() });
            }
        }
    }

    /**
     * Process all the posible customizations on the given structure and nested structures.
     *
     * @param structure StructureDef
     */
    private void customize(final StructureDef structure) {
        if (customizers.isEmpty()) {
            //Do nothing if there are no customizations.
            return;
        }

        final Collection<Object> custs = customizers.get(structure.getFullName());
        if (custs != null && !custs.isEmpty()) {
            for (Object cust : custs) {
                apply(cust, structure);
            }
        }

        //Recursive customizations:
        customizeAccessors(structure.getAccessorDefinitions());
        customizeMethodReturns(structure.getOperationDefinitions());
    }

    /**
     * Apply the customization class on the structure.
     *
     * @param customization The customization to apply.
     * @param structure The structure to apply the customization on.
     */
    private void apply(Object customization, StructureDef structure) {
        unusedCustomizers.remove(customization);
        for (Method method : customization.getClass().getMethods()) {
            for (Map.Entry<Class<? extends Annotation>, Adopter> entry : adopters.entrySet()) {
                Class<? extends Annotation> annoClass = entry.getKey();
                Adopter adopter = entry.getValue();

                final Annotation annotation = method.getAnnotation(annoClass);
                if (annotation != null) {
                    try {
                        adopter.checkMethodSignature(method, annotation, structure);
                    } catch (InvalidMethodSignatureException e) {
                        // TODO: better handling. At runtime this can fail but at
                        // design time in JDev this should give an informative message
                        // without crashing JDev
                        throw new RuntimeException(e);
                    }
                    adopter.adjustStructure(structure, annotation, method);
                    Collection<? extends Handler> handlers = adopter.createHandlers(annotation, method, customization);
                    if (handlers != null && !handlers.isEmpty()) {
                        HandlerRegistry registryHelper = new HandlerRegistry(structure);
                        for (Handler handler : handlers) {
                            registryHelper.add(handler);
                        }
                    }
                }
            }
        }
    }

}
