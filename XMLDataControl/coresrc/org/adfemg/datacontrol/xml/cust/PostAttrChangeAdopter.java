package org.adfemg.datacontrol.xml.cust;

import java.lang.reflect.Method;

import java.util.Collection;
import java.util.Collections;

import oracle.adf.model.adapter.dataformat.StructureDef;

import org.adfemg.datacontrol.xml.annotation.PostAttrChange;
import org.adfemg.datacontrol.xml.events.AttrChangeEvent;
import org.adfemg.datacontrol.xml.handler.PostPutHandler;


/**
 * The Adporter for the PostAttrChange annotation.
 *
 * @see Adopter
 * @see PostAttrChange
 */
public class PostAttrChangeAdopter implements Adopter<PostAttrChange> {
    /**
     * Check the method signature of the PostAttrChange method.
     * The method should start with the attribute name and end with 'Changed'.
     *
     * @inheritDoc
     */
    @Override
    public void checkMethodSignature(Method method, PostAttrChange annotation,
                                     StructureDef structure) throws InvalidMethodSignatureException {
        AdopterUtil.checkSignature(method, Void.class, AttrChangeEvent.class);
        AdopterUtil.checkMethodEndsWith(method, "Changed");
    }

    @Override
    public void adjustStructure(StructureDef structure, PostAttrChange annotation, Method method) {
        //No need to adjust the structure, the invoke needs to be fired postPut attrChange.
        //Call is done in the put of the XMLDCElement.

    }

    /**
     * Creates a PostPutHandler that fires the method created in the
     * customization class an AttrChangeEvent as arguments.
     *
     * @see PostPutHandler
     * @see AttrChangeEvent
     * @inheritDoc
     */
    @Override
    public Collection<PostPutHandler> createHandlers(final PostAttrChange annotation, final Method method,
                                                     final Object customizer) {
        PostPutHandler handler = new PostPutHandler() {
            final String attrName = getAttributeName(method);

            @Override
            public void doPostPut(AttrChangeEvent attrChangeEvent) {
                AdopterUtil.invokeMethod(customizer, method, attrChangeEvent);
            }

            @Override
            public boolean handlesAttribute(String input) {
                return attrName.equals(input);
            }
        };
        return Collections.singletonList(handler);
    }

    private String getAttributeName(Method method) {
        return AdopterUtil.getAttributeName(method, "Changed", false);
    }
}
