package org.adfemg.datacontrol.xml.cust;

/**
 * The invalid method signature exception is created so this exception can
 * be thrown by the adopters if the signature of the method is incorrect.
 */
public class InvalidMethodSignatureException extends Exception {
    @SuppressWarnings("compatibility:-4258701039256175003")
    private static final long serialVersionUID = 1L;

    public InvalidMethodSignatureException(Throwable throwable) {
        super(throwable);
    }

    public InvalidMethodSignatureException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public InvalidMethodSignatureException(String string) {
        super(string);
    }

    public InvalidMethodSignatureException() {
        super();
    }
}
