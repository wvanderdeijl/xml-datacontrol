package org.adfemg.datacontrol.xml.cust;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import java.util.Collection;

import org.adfemg.datacontrol.xml.handler.Handler;

import oracle.adf.model.adapter.dataformat.StructureDef;


/**
 * Responsible for applying a datacontrol annotation by changing the datacontrol
 * structure as well as supplying the runtime handlers that implement the
 * annotation behavior at runtime.
 *
 * @param <T> needs to be of the type Annotation.
 * @see Annotation
 */
public interface Adopter<T extends Annotation> {
    /**
     * Checks if the annotated method has the correct signature (return type and
     * argument types).
     *
     * @param method Java method being annotated with the give annotation.
     * @param annotation Datacontrol annotation.
     * @param structure Datacontrol structure being targeted by the customization
     *                  class.
     * @throws InvalidMethodSignatureException if the method signature is invalid.
     */
    void checkMethodSignature(Method method, T annotation,
                              StructureDef structure) throws InvalidMethodSignatureException;

    /**
     * Allows for the Adopter to alter the structure of a datacontrol element
     * based on an annotation. If an adopter does not need to change the structure
     * of the datacontrol this can simply be an empty method.
     *
     * @param structure Datacontrol structure being targeted by the customization
     *                  class.
     * @param annotation Datacontrol annotation.
     * @param method Java method being annotated with the give annotation.
     */
    void adjustStructure(StructureDef structure, T annotation, Method method);

    /**
     * Returns a list of runtime handlers that implement the behavior of this
     * annotation. These handlers will be invoked by the datacontrol at the
     * appropriate times based on the type of handler.
     *
     * @param annotation Datacontrol annotation.
     * @param method     Java method being annotated with the give annotation.
     * @param customizer Customization class instance that has the annotated
     *                   method that should be invoked by one of the returned
     *                   handlers.
     * @return Collection of handlers.
     * @see Handler
     */
    Collection<? extends Handler> createHandlers(T annotation, Method method, Object customizer);

}
