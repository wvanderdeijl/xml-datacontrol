package org.adfemg.datacontrol.xml;

import java.beans.Beans;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import oracle.adf.share.ADFContext;
import oracle.adf.share.Environment;
import oracle.adf.share.logging.ADFLogger;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDBuilder;
import oracle.xml.parser.schema.XSDException;

import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.cache.CacheProvider;
import org.adfemg.datacontrol.xml.provider.customization.CustomizationProvider;
import org.adfemg.datacontrol.xml.provider.data.DataProvider;
import org.adfemg.datacontrol.xml.provider.structure.StructureProvider;
import org.adfemg.datacontrol.xml.provider.transform.TransformationProvider;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.Utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Configuration class of the XML DataControl. This should be initialized with the
 * &lt;Source&gt; node from the DataControls.dcx configuration file. This Node can
 * be supplied through the constructor or by caling {@link #load}. After loading
 * of this source node, several getter methods are available to read this metadata.
 */
public class DataControlDefinitionNode {
    public static final String NS_DEFINITION = "http://adfemg.org/adfm/datacontrol/configuration";

    public static final String ELEM_DEFINITION = "definition";
    public static final String DEF_ATTR_SCHEMA = "schema";
    public static final String DEF_ATTR_SCHEMA_DT = "schema-dt";
    public static final String DEF_ATTR_SCHEMA_ROOT = "schema-root";
    public static final String DEF_ATTR_DC_OPERATION = "dc-operation";

    // When adding a new type of provider, be sure to also update XMLDCConstants with the
    // default implementation
    public static final String ELEM_DATA_PROVIDER = "data-provider";
    public static final String ELEM_TRANSFORM_PROVIDER = "transformation-provider";
    public static final String ELEM_CACHE_PROVIDER = "cache-provider";
    public static final String ELEM_MAPPER_PROVIDER = "type-mapper";
    public static final String ELEM_CUST_PROVIDER = "customization-provider";
    public static final String ELEM_STRUCT_PROVIDER = "structure-provider";
    public static final String PROV_ATTR_CLASS = "class";

    public static final String ELEM_PARAMETERS = "parameters";

    //Parameter Constants
    public static final String ELEM_PARAM = "parameter";
    public static final String PARAM_ATTR_NAME = "name";
    public static final String PARAM_ATTR_VALUE = "value";

    //Dynamic Parameter Constants
    public static final String ELEM_DYNAMIC_PARAM = "dynamic-parameter";
    public static final String PARAM_DYNAMIC_ATTR_NAME = "name";
    public static final String PARAM_DYNAMIC_ATTR_TYPE = "java-type";

    //XML Parameter Constants
    public static final String ELEM_XML_PARAM = "xml-parameter";
    public static final String PARAM_XML_ATTR_NAME = "name";

    //List Parameter Constants
    public static final String ELEM_LIST_PARAM = "list-parameter";
    public static final String PARAM_LIST_ATTR_NAME = "name";
    public static final String VALUE_ELEM_LIST_PARAM = "value";

    private static final ADFLogger logger = ADFLogger.createADFLogger(DataControlDefinitionNode.class);

    // regexp pattern om "variabelen" in de waarde van attributen te vinden
    // zodat we deze kunnen vervangen door context init parameters
    private static final Pattern REPLACE_PATTERN = Pattern.compile(".*?(\\$\\{)([^}]*)(\\}).*", Pattern.DOTALL);

    /**
     * Default name that will be used for the datacontrol operation if none is specified
     * in the configuration XML.
     */
    public static final String DFLT_DC_OPERATION = "getXML";

    private DataControlDefinition dcDef;

    // properties from DataControls.dcx
    private String schema;
    private String schemaDesignTime;
    private String schemaRoot;
    private String datacontrolOperation;
    private Map<Class<? extends Provider>, DataControlProviderDefinition> providerDefs =
        new HashMap<Class<? extends Provider>, DataControlProviderDefinition>(); // provider definitions
    private Map<Class<? extends Provider>, Provider> providerInstances =
        new HashMap<Class<? extends Provider>, Provider>(); // provider instances

    // cached values
    private URL url;
    private XMLSchema fetchedSchema;

    /**
     * Default no-args constructor.
     */
    public DataControlDefinitionNode() {
    }

    /**
     * Constructor with the &lt;Source&gt; node from the DataControls.dcx file.
     * Loads the definition from the Node.
     *
     * @param node The &lt;Source&gt; node from the DataControls.dcx file.
     */
    public DataControlDefinitionNode(final DataControlDefinition dcDef,final Node node) {
        this.dcDef = dcDef;
        load(node);
    }

    /**
     * Returns the DataControlDefinition this definitionNode belongs to, or null
     * if this DataControlDefinitionNode was created using the no-arg constructor.
     * @return DataControlDefinition
     */
    public DataControlDefinition getDataControlDefinition() {
        return dcDef;
    }

    /**
     * Load method to load (re-)initialise a definition from a Node.
     *
     * @param node The <code>{http://adfemg.org/adfm/datacontrol/configuration}definition</code>
     *             node from the DataControls.dcx file.
     */
    public void load(final Node node) {
        // (re-)initialise
        schema = null;
        schemaRoot = null;
        datacontrolOperation = null;
        providerDefs.clear();
        url = null;

        if (!(node instanceof Element) || !NS_DEFINITION.equals(node.getNamespaceURI()) ||
            !ELEM_DEFINITION.equals(node.getLocalName())) {
            return;
        }
        loadDefinition((Element) node);
    }

    /**
     * Get the information from the Element and put them in the variables.
     * @param definition The <code>{http://adfemg.org/adfm/datacontrol/configuration}definition</code>
     *                   Element containing the definition information.
     */
    private void loadDefinition(final Element definition) {
        schema = getOptionalAttribute(definition, DEF_ATTR_SCHEMA);
        schemaDesignTime = getOptionalAttribute(definition, DEF_ATTR_SCHEMA_DT);
        schemaRoot = getOptionalAttribute(definition, DEF_ATTR_SCHEMA_ROOT);
        datacontrolOperation = getOptionalAttribute(definition, DEF_ATTR_DC_OPERATION);
        // when adding a new type of provider, be sure to also update XMLDCConstants.DFLT_PROVIDERS
        // TODO: perhaps it is better to include the defaults in the call to loadProviderDef so we
        // force developers to think about default here. Could very well be that defaults still come
        // from central map.
        providerDefs.put(DataProvider.class, loadProviderDef(definition, ELEM_DATA_PROVIDER));
        providerDefs.put(TransformationProvider.class, loadProviderDef(definition, ELEM_TRANSFORM_PROVIDER));
        providerDefs.put(CacheProvider.class, loadProviderDef(definition, ELEM_CACHE_PROVIDER));
        providerDefs.put(TypeMapper.class, loadProviderDef(definition, ELEM_MAPPER_PROVIDER));
        providerDefs.put(CustomizationProvider.class, loadProviderDef(definition, ELEM_CUST_PROVIDER));
        providerDefs.put(StructureProvider.class, loadProviderDef(definition, ELEM_STRUCT_PROVIDER));
    }

    /**
     * Loads the providers out of the provided Element based on the ElementName
     * from the input parameters.
     *
     * @param definition The element containing the definition.
     * @param providerElemName The element name to look for in the definition.
     * @return The DataControlProvider.
     */
    private DataControlProviderDefinition loadProviderDef(final Element definition, final String providerElemName) {
        Element provElem = Utils.findFirstChildElement(definition, providerElemName);
        if (provElem == null) {
            return null;
        }
        String className = getOptionalAttribute(provElem, PROV_ATTR_CLASS);
        DataControlProviderDefinition provider = new DataControlProviderDefinition(className);
        Element params = Utils.findFirstChildElement(provElem, ELEM_PARAMETERS);
        if (params != null) {
            for (Element param :
                 Utils.findChildElements(params,
                                         ELEM_PARAM)) { //Loop over de gewone params en set deze.
                provider.setParameter(getOptionalAttribute(param, PARAM_ATTR_NAME),
                                      getOptionalAttribute(param, PARAM_ATTR_VALUE));
            }
            for (Element param : Utils.findChildElements(params, ELEM_XML_PARAM)) { //Loop over de XML params en set deze.
                String name = getOptionalAttribute(param, PARAM_XML_ATTR_NAME);
                Node n = param.getFirstChild();
                if (n != null) {
                    if (n.getNodeType() == Node.TEXT_NODE || n.getNodeType() == Node.CDATA_SECTION_NODE) {
                        provider.setParameter(name, Utils.parse(n.getNodeValue()).getDocumentElement());
                    } else {
                        logger.warning("xml parameter {0} in DataControls.dcx should use <![CDATA[....]]> for proper namespace handling, not {1} with value {2}", new Object[] {
                                       name, n, n.getNodeValue()
                        });
                        provider.setParameter(name, n.cloneNode(true));
                    }
                }
            }
            for (Element param : Utils.findChildElements(params, ELEM_DYNAMIC_PARAM)) {
                provider.setDynamicParam(getOptionalAttribute(param, PARAM_DYNAMIC_ATTR_NAME),
                                         getOptionalAttribute(param, PARAM_DYNAMIC_ATTR_TYPE));
            }
            for (Element param : Utils.findChildElements(params, ELEM_LIST_PARAM)) {
                String name = getOptionalAttribute(param, PARAM_LIST_ATTR_NAME);
                List<String> values = new ArrayList<String>();
                for (Element value : Utils.findChildElements(param, VALUE_ELEM_LIST_PARAM)) {
                    values.add(value.getTextContent());
                }
                provider.setParameter(name, values);
            }
        }
        return provider;
    }

    /**
     * Get an attribute by its name from the Element.
     *
     * @param element The element containint the attribute.
     * @param attrName The name of the attribute to look for in the element.
     * @return The attribute as String or <code>null</code> if the attribute did not exist or
     *         contained an empty string.
     */
    private String getOptionalAttribute(final Element element, final String attrName) {
        String ret = element.getAttribute(attrName);
        if (ret == null || ret.trim().isEmpty()) {
            return null;
        }
        return resolveExpressions(ret);
    }

    private String resolveExpression(final String expression) {
        if (!ADFContext.hasCurrent()) { // No context availeable, we are prob. in the design mode of JDev.
            return null;
        }
        ADFContext adfCtxt = ADFContext.getCurrent();
        Environment env = adfCtxt.getEnvironment();
        Object ctxt = env == null ? null : env.getContext();
        if (ctxt instanceof ServletContext) {
            ServletContext sCtxt = (ServletContext) ctxt;
            return sCtxt.getInitParameter(expression);
        }
        return null;
    }

    private String resolveExpressions(final String value) {
        if (value == null) {
            return null;
        }
        // TODO: We should prob. re-write this to a use EL resolving.
        String retval = value;
        Matcher matcher = REPLACE_PATTERN.matcher(retval);
        while (matcher.matches()) {
            String prefix = retval.substring(0, matcher.start(1));
            String newVal = resolveExpression(matcher.group(2));
            String postfix = retval.substring(matcher.end(3), retval.length());
            retval = prefix + newVal + postfix;
            matcher = REPLACE_PATTERN.matcher(retval);
        }
        return retval;
    }

    /**
     * We can not return the Metadata (XML) in its original state as placeholders in
     * provider parameters have already been replaced with runtime values.
     *
     * @return always throws an UnsupportedOperationException.
     */
    public Node createMetadata() {
        throw new UnsupportedOperationException("createMetadata is not supported in the XMLDataControl");
    }

    /**
     * Get the URL of the schema.
     * First we try if the schema is an URL, else we try if the Schema is a
     * relative path to a file and to create a File. If this also failes we
     * try to load the schema as a resource.
     *
     * @return the URL, <code>null</code> if all tries fail.
     */
    public URL getSchemaUrl() {
        if (url != null || schema == null) {
            return url;
        }
        String resolvedSchema = Beans.isDesignTime() && schemaDesignTime != null ? schemaDesignTime : schema;
        // Try if the schema is a URL
        if (url == null) {
            try {
                url = new URL(resolvedSchema);
            } catch (MalformedURLException e) {
                url = null;
            }
        }
        // Try if the Schema is a relative path to a file.
        if (url == null) {
            File file = new File(resolvedSchema);
            if (file.canRead()) {
                try {
                    url = file.toURI().toURL();
                } catch (MalformedURLException e) {
                    url = null;
                }
            }
        }
        // Try to load the Schema as a resource.
        if (url == null) {
            ClassLoader cloader = Utils.getWorkspaceClassLoader();
            url = cloader.getResource(resolvedSchema); // Give null if resource not found.
            if (url == null && resolvedSchema.startsWith("/")) {
                url = cloader.getResource(resolvedSchema.substring(1));
            }
        }
        return url;
    }

    /**
     * Get the actual XML Schema. Will cache the result so subsequent calls
     * will just return the cached schema. When the schema cannot be loaded,
     * this will throw an exception at runtime or show a dialog at design
     * time.
     * @return the XMLSchema, which might be cached from a previous fetch.
     * @see #getSchemaUrl
     */
    public XMLSchema getSchema() throws XSDException {
        if (fetchedSchema != null) {
            return fetchedSchema;
        }
        Utils.ensureMdsHandlers();
        final XSDBuilder builder = new XSDBuilder();
        if (logger.isConfig()) {
            final HashMap<String, String> context = new HashMap<String, String>();
            context.put("schemaUrl", String.valueOf(getSchemaUrl()));
            logger.begin("fetching XSD", context);
        }
        try {
            URL schemaUrl = getSchemaUrl();
            if (schemaUrl == null) {
                if (Beans.isDesignTime()) {
                    MessageUtils.showErrorMessage("Loading Exception",
                                                  "Failed to load XML Schema as URL, File or Classpath Resource. " +
                                                  "\nPlease check the schema-dt in the definition node in the DataControls.dcx " +
                                                  "file and make sure the XSD can be found. \nWhen using the classpath Resource, " +
                                                  "first compile your project so the files exist in the classes directory.");
                    return null;
                } else {
                    throw new IllegalArgumentException("Failed to load XML Schema as URL, File or Classpath Resource. Check the schema in the definition node in the DataControls.dcx file.");
                }
            }
            fetchedSchema = builder.build(schemaUrl);
            return fetchedSchema;
        } finally {
            logger.end("fetching XSD");
        }
    }

    /**
     * Get the providers.
     * @return an unmodifiableMap of the providers.
     */
    public Map<Class<? extends Provider>, DataControlProviderDefinition> getProviderDefinitions() {
        return Collections.unmodifiableMap(providerDefs);
    }

    /**
     * Get an instantiated provider of a certain type, or otherwise instantiate it
     * and cache it for future reference.
     * @param <T>
     * @param iface
     * @return
     */
    public <T extends Provider> T getProviderInstance(Class<T> iface) {
        if (!providerInstances.containsKey(iface)) {
            Class dfltImpl = XMLDCConstants.DFLT_PROVIDERS.get(iface);
            if (dfltImpl != null) {
                final DataControlProviderDefinition defFromDcx = getProviderDefinitions().get(iface);
                final Provider impl = newProviderInstance(defFromDcx, iface, dfltImpl);
                providerInstances.put(iface, impl);
            }
        }
        return (T) providerInstances.get(iface);
    }

    /**
     * Returns all already instantiated providers. Remember that this doesn't have
     * to be all providers that are defined in DataControls.dcx. If you want to
     * force loading of a defined provider, use getProviderInstace(Class)
     * @return all instantiated providers for this DataControlDefinitionNode
     */
    public Collection<Provider> getProviderInstances() {
        return Collections.unmodifiableCollection(providerInstances.values());
    }

    /**
     * Instantiate a (new) provider and set all parameters on this provider.
     *
     * @param <T> Type of the (new) provider.
     * @param providerDef Definition of the (new) provider.
     * @param iface The interface to implement by the (new) provider.
     * @param defaultClass The default Class to use if the <code>providerDef</code>
     *                     does not contain a Class.
     * @return Instance of the <code>iface</code>
     */
    private <T extends Provider> T newProviderInstance(final DataControlProviderDefinition<T> providerDef,
                                                       final Class<T> iface, final Class<? extends T> defaultClass) {
        Class<? extends T> cls = null;
        if (providerDef != null) {
            cls = providerDef.loadClass(iface);
        }
        if (cls == null) {
            cls = defaultClass;
        }
        T provider = Utils.newInstance(cls);
        if (providerDef != null) {
            Map<String, Object> params = providerDef.getParameters();
            if (params != null) {
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    provider.setParameter(param.getKey(), param.getValue());
                }
            }
        }
        return provider;
    }

    /**
     * Get the Schema Root.
     * @return The schemaRoot as String.
     */
    public String getRoot() {
        return schemaRoot;
    }

    /**
     * Gets the name of the operation that should be exposed on the datacontrol.
     * @return value of the dc-operation attribute in the DataControls.dcx configuration or
     *         {@link #DFLT_DC_OPERATION} if this wasn't specified.
     */
    public String getDatacontrolOperation() {
        return datacontrolOperation != null ? datacontrolOperation : DFLT_DC_OPERATION;
    }

}
