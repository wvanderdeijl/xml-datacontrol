package org.adfemg.datacontrol.xml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import oracle.adf.model.adapter.AdapterException;

import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.utils.Utils;


/**
 * The Datacontrol Provider.
 *
 * @param <T> The Type of Provider.
 *
 * @see Provider
 * @see ProviderImpl
 */
public class DataControlProviderDefinition<T extends Provider> extends ProviderImpl {
    //Map to put the dynamic parameters on.
    private Map<String, String> dynamicParams = new HashMap<String, String>();

    private String className;

    /**
     * Public constructor with field argument for className.
     * @param className the class name as String.
     */
    public DataControlProviderDefinition(final String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    /**
     * Set a parameter n the dynamicParameterMap.
     * @param name key
     * @param value value
     */
    public void setDynamicParam(final String name, final String value) {
        dynamicParams.put(name, value);
    }

    /**
     * Get a dynamic parameter from the Map, based on the name (key).
     * @param name key
     * @return the value from the map.
     */
    public String getDynamicParam(final String name) {
        return dynamicParams.get(name);
    }

    /**
     * Get the whole map with dynamicParameters.
     * @return The dynamicParameters as unmodifiableMap.
     */
    public Map<String, String> getDynamicParams() {
        return Collections.unmodifiableMap(dynamicParams);
    }

    /**
     * Loads the class that is set on the instance if it implements the interface
     * that is given as input argument to this method.
     *
     * @param iface The interface the class needs to implement.
     * @return The resulting Class object.
     */
    public Class<? extends T> loadClass(final Class<T> iface) {
        if (className == null) {
            return null;
        }
        try {
            Class<?> cls = Utils.getWorkspaceClassLoader().loadClass(className);
            if (!iface.isAssignableFrom(cls)) {
                throw new IllegalArgumentException(className + " does not implement interface " + iface.getName());
            }
            return (Class<? extends T>) cls;
        } catch (ClassNotFoundException e) {
            throw new AdapterException(e);
        }
    }
}
