package org.adfemg.datacontrol.xml.events;

import java.util.EventListener;

/**
 * Interface for ValidationListener.
 *
 * This listener will be fired in the DataControl on validation time.
 * After succesfull validation the listeners will be removed from the DataControl.
 *
 * @see EventListener
 */

public interface ValidationListener extends EventListener {
    void validate();
}
