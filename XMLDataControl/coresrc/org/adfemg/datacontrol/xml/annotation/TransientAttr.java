package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation to define a transient attribute on the DataControl. The
 * name and data type of the transient attribute are determined by the
 * signature of the annotated method.
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public void init<i>AttrName</i>(XMLDCElement department)</code>
 * <p>
 * The return value from this method will be used to set as initial value for
 * the transient attribute, to define an empty transient attribute, return
 * <code>null</code>.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface TransientAttr {
}
