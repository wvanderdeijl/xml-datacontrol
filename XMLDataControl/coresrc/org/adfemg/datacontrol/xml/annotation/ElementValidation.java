package org.adfemg.datacontrol.xml.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static org.adfemg.datacontrol.xml.annotation.AnnotationHelper.DFLT_ATTR_STRING;


/**
 * Use this annotation to define a element validation.
 * <p>
 * The signature of the annotated method has to be:<br/>
 * <code>public void someMethod(XMLDCElement department)</code>
 * <p>
 * If there is no list of attributes defined on the annotation attr attribute,
 * the validation will be fired for any changed attribute.
 * If there is a list of attributes defined on the attr attribute, the validation
 * will be fired only if an attribute in this list is updated.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface ElementValidation {
    //For some reason, the static import gets rid of an 'incompatable type' error.
    String[] attr() default DFLT_ATTR_STRING;
}
