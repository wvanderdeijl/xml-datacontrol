package org.adfemg.datacontrol.xml.design;

import oracle.ide.Ide;
import oracle.ide.model.Project;
import oracle.ide.model.Reference;
import oracle.ide.model.Workspace;

import oracle.jdeveloper.webapp.utils.ProjectRunClassPathClassLoaderUtils;

/**
 * Warning: this class introduces a dependency on JDeveloper design time classes.
 * Be sure to check java.beans.Beans#isDesignTime before invoking anything from
 * this class to prevent ClassNotFoundException's on design time classes.
 */
public class ClassLoaderUtil {

    private ClassLoaderUtil() {
    }

    public static ClassLoader getDesignTimeClassLoader() {
        ClassLoader retval = Thread.currentThread().getContextClassLoader();
        // During design time we need to get all the classloaders from the active workspace
        final Workspace activeWorkspace = Ide.getActiveWorkspace();
        for (Object child : activeWorkspace.getListOfChildren()) {
            if (child instanceof Reference) {
                oracle.ide.model.Node node = ((Reference) child).getData();
                if (node instanceof Project) {
                    final Project project = (Project) node;
                    // Create a child classloader for the project with the current classloader as parent.
                    retval = ProjectRunClassPathClassLoaderUtils.getClassLoader(project, retval);
                }
            }
        }
        return retval;
    }

}
