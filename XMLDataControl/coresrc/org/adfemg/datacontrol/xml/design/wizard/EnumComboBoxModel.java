package org.adfemg.datacontrol.xml.design.wizard;

import javax.swing.DefaultComboBoxModel;

// FIXME use generics in JDK 1.7 (eg JDeveloper 12c)
public class EnumComboBoxModel<T extends Enum> extends DefaultComboBoxModel {
//public class EnumComboBoxModel<T extends Enum> extends DefaultComboBoxModel<T> {

    public EnumComboBoxModel(Class<T> cls) {
        super(cls.getEnumConstants());
    }

}
