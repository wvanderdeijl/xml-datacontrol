package org.adfemg.datacontrol.xml.design;

import oracle.ide.Ide;

import oracle.javatools.dialogs.MessageDialog;

/**
 * Simplified wrapper for oracle.javatools.dialogs.MessageDialog that only
 * exposes method we actually need. Without this utility class the caller
 * might want to invoke any of the static methods in oracle.javatools.dialogs.MessageDialog
 * directly. Downside to that is that these need a parent component for which the
 * caller would need to invoke oracle.ide.Ide#getMainWindow. This introduces
 * a dependency on the oracle.ide.Ide class which might not be available at
 * runtime, for example when running the datacontrol tester.
 * With this DialogUtil class the caller can first check for Beans.isDesignTime()
 * which is always available and only call one of our methods when running in
 * design time and the Ide class is available. When not running in design-time
 * the caller can prevent calling our class and thus remove the dependency
 * on oracle.ide.Ide
 * <p>Warning: this class introduces a dependency on JDeveloper design time classes.
 * Be sure to check java.beans.Beans#isDesignTime before invoking anything from
 * this class to prevent ClassNotFoundException's on design time classes.
 */
public class MessageUtils {

    public static void showErrorMessage(String title, String errorMessage) {
        MessageDialog.error(Ide.getMainWindow(), errorMessage, title, null);
    }

    public static void showInformationMessage(String title, String errorMessage) {
        MessageDialog.information(Ide.getMainWindow(), errorMessage, title, null);
    }
}
