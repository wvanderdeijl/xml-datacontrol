package org.adfemg.datacontrol.xml.handler;

/**
 * A specific interface for handlers that handle attributes.
 */
public interface AttributeHandler extends Handler {
    /**
     * Implement this method as a check to see wether the handler should be concerned
     * about the attribute we're checking.
     *
     * @param attrName the attribute name to check for.
     * @return <code>true</code> if the handler is handling this attribute,
     *         <code>false</code> if the handler is not handling this attribute.
     */
    boolean handlesAttribute(String attrName);
}
