package org.adfemg.datacontrol.xml.provider.customization;

import java.util.ArrayList;
import java.util.Collection;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.utils.Utils;


/**
 * The Implementation of the CustomizationProvider.
 *
 * @see CustomizationProvider
 * @see ProviderImpl
 * @see CustomizationProvider
 */
public class CustomizationProviderImpl extends ProviderImpl implements CustomizationProvider {
    private static final ADFLogger logger = ADFLogger.createADFLogger(CustomizationProviderImpl.class);

    public static final String PARAM_CLASSES = "classes";

    public CustomizationProviderImpl() {
        super();
    }

    /**
     * Method to get all the customizers.
     *
     * @return A collection of customizers.
     */
    public Collection<? extends Object> getCustomizers() {
        // FIXME: If this method is called from the DataControl Panel in JDeveloper before
        //        the Customization Class has been compiled, there will be an JDeveloper Exception.
        //        We need to replace this with a human friendly error, possibly with the instruction
        //        to compile the customization class.
        final Object classNames = getParameter(PARAM_CLASSES);
        if (!(classNames instanceof Collection)) {
            throw new IllegalArgumentException("CustomizationProviderImpl needs list-parameter 'classes'");
        }

        final Collection<Object> retval = new ArrayList<Object>();
        final ClassLoader classLoader = Utils.getWorkspaceClassLoader();
        for (Object className : (Collection) classNames) {
            if (!(className instanceof String)) {
                throw new IllegalArgumentException("The list-parameter within classes needs to be a Sting.");
            }
            try {
                final Class<?> clazz = classLoader.loadClass((String) className);
                retval.add(clazz.newInstance());
            } catch (Exception e) {
                logger.severe(e);
                throw new IllegalArgumentException("Can not load " + className, e);
            }
        }
        return retval;
    }

}
