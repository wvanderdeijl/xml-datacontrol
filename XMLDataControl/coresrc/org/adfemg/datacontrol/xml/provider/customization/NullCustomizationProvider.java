package org.adfemg.datacontrol.xml.provider.customization;

import java.util.Collection;
import java.util.Collections;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;


/**
 * The Implementation of a null CustomizationProvider.
 * Will always returns a emptySet for the customizers.
 *
 * @see CustomizationProvider
 * @see ProviderImpl
 * @see CustomizationProvider
 */
public class NullCustomizationProvider extends ProviderImpl implements CustomizationProvider {
    public NullCustomizationProvider() {
        super();
    }

    /**
     * The getCustomizers method.
     * @return will always return an emptySet.
     */
    public Collection<Class<? extends Object>> getCustomizers() {
        return Collections.emptySet();
    }
}
