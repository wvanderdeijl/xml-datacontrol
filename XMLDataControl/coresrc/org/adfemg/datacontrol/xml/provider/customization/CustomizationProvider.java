package org.adfemg.datacontrol.xml.provider.customization;

import java.util.Collection;

import org.adfemg.datacontrol.xml.provider.Provider;


/**
 * The provider of the customization class for the customization off a datacontrol.
 */
public interface CustomizationProvider extends Provider {

    /**
     * Getter for the customization classes for the datacontrol.
     * @return Collection of objects of which the classes have the annotation @ElementCustomization.
     */
    Collection<? extends Object> getCustomizers();

}
