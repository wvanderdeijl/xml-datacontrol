package org.adfemg.datacontrol.xml.provider;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.adfemg.datacontrol.xml.DataControl;


/**
 * Abstract class implementing the Provider.
 *
 * @see Provider
 */
public abstract class ProviderImpl implements Provider {
    private Map<String, Object> params = new HashMap<String, Object>();

    /**
     * Default no-args constructor.
     */
    public ProviderImpl() {
        super();
    }

    @Override
    public void setParameter(final String name, final Object value) {
        params.put(name, value);
    }

    public Object getParameter(final String name) {
        return params.get(name);
    }

    public Map<String, Object> getParameters() {
        return Collections.unmodifiableMap(params);
    }

    public void dataControlCreated(final DataControl dc) {
        // NOOP
    }

    public String getParameterAsString(final String name) {
        Object o = params.get(name);
        return o == null ? null : String.valueOf(o);
    }
}
