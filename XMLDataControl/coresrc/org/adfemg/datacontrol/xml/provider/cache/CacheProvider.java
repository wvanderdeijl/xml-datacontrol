package org.adfemg.datacontrol.xml.provider.cache;

import java.util.Map;

import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.provider.Provider;

/**
 * The CacheProvider is used by the DataControl to get any cached version of the root
 * XML element before invoking the DataProvider.
 * <p>
 * The default implementation is {@link NoCacheProvider}. Custom implementations could
 * use LRUMap from Apache Commons Collections to store and retrieve values. LRUMap
 * automatically orders the map by how recent the entries have been used. Getting an
 * entry from a LRUMap already moves it to the most recently used side. It also
 * automatically trims the map when it exceeds its maximum size.
 * @see NoCacheProvider
 */
public interface CacheProvider extends Provider {

    /**
     * Stores a XML element in the cache.
     * @param structDefName Name of the StructureDefinition which is used to distinguish the caches
     *                      of different structures and datacontrols.
     * @param params Map of named value pairs of arguments and argument values that were passed
     *               to the bound operation that retrieved the XML element. Can be an empty map
     *               for dataControl operations without dynamic parameters. This map is to be used
     *               as key in the cache as a map with similar values will be used to retrieve
     *               the element from the cache with @{#get}
     * @param element XML datacontrol element to cache
     */
    void put(String structDefName, Map params, XMLDCElement element);

    /**
     * Gets a (previously cached) element from the cache.
     * @param structDefName Name of the StructureDefinition which is used to distinguish the caches
     *                      of different structures and datacontrols.
     * @param params Map of named value pairs of arguments and argument values that were passed
     *               to the bound operation that retrieved the XML element. Can be an empty map
     *               for dataControl operations without dynamic parameters.
     * @return cached datacontrol element or <code>null</code> if no element was cached for the
     *         given parameter values
     */
     XMLDCElement get(String structDefName, Map params);

}
