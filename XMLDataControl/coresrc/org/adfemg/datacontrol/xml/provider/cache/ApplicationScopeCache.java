package org.adfemg.datacontrol.xml.provider.cache;

import java.util.Map;

import oracle.adf.share.ADFContext;

/**
 * Can be used as a base class for an caching provider that stores the cache
 * at application-scope making it a shared cache for all users.
 * The only method that has to be implemented by the subclass is createCache
 * which could typically just instantiate a LRUMap from Apache Commons Collections.
 */
public abstract class ApplicationScopeCache extends AdfScopeCache {

    @Override
    protected Map<String, Object> getScope() {
        return ADFContext.getCurrent().getApplicationScope();
    }

}
