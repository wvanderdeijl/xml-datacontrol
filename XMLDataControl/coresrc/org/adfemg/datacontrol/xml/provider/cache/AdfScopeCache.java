package org.adfemg.datacontrol.xml.provider.cache;

import java.util.Map;

import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;

public abstract class AdfScopeCache extends ProviderImpl implements CacheProvider {

    protected abstract Map<String, Object> getScope();

    protected abstract Map<Map, XMLDCElement> createCache();

    protected String getScopeKey(String structDefName) {
        return this.getClass().getName() + "." + structDefName + ".CacheStore";
    }

    protected synchronized Map<Map, XMLDCElement> getCache(String structDefName) {
        Map<String, Object> scope = getScope();
        String scopeKey = getScopeKey(structDefName);
        if (!scope.containsKey(scopeKey)) {
            scope.put(scopeKey, createCache());
        }
        return (Map<Map, XMLDCElement>) scope.get(scopeKey);
    }

    @Override
    public synchronized void put(String structDefName, Map params, XMLDCElement element) {
        getCache(structDefName).put(params, element);
    }

    @Override
    public synchronized XMLDCElement get(String structDefName, Map params) {
        return getCache(structDefName).get(params);
    }

}
