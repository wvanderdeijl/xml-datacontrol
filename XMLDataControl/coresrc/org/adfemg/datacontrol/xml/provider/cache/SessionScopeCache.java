package org.adfemg.datacontrol.xml.provider.cache;

import java.util.Map;

import oracle.adf.share.ADFContext;

/**
 * Can be used as a base class for an caching provider that stores the cache
 * at session-scope making it a shared cache for all taskflows in a single user
 * session.
 * The only method that has to be implemented by the subclass is createCache
 * which could typically just instantiate a LRUMap from Apache Commons Collections.
 */
public abstract class SessionScopeCache extends AdfScopeCache {

    @Override
    protected Map<String, Object> getScope() {
        return ADFContext.getCurrent().getSessionScope();
    }

}
