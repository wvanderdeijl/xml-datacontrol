package org.adfemg.datacontrol.xml.provider.cache;

import java.util.Map;

import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;

public class NoCacheProvider extends ProviderImpl implements CacheProvider {

    @Override
    public void put(String structDefName, Map params, XMLDCElement element) {
        // noop
    }

    @Override
    public XMLDCElement get(String structDefName, Map params) {
        return null;
    }

}
