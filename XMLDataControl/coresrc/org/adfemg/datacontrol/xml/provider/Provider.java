package org.adfemg.datacontrol.xml.provider;

import org.adfemg.datacontrol.xml.DataControl;

/**
 * Provider interface.
 */
public interface Provider {
    // after create provider
    void setParameter(String name, Object value);

    // after create dataControl
    void dataControlCreated(DataControl dc);
}
