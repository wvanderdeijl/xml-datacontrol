package org.adfemg.datacontrol.xml.provider.data.ws;

import java.beans.Beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

import org.adfemg.datacontrol.xml.design.MessageUtils;
import org.adfemg.datacontrol.xml.provider.data.WSDataProvider;
import org.adfemg.datacontrol.xml.utils.Utils;

public class HandlerResolverImpl implements HandlerResolver {

    private final WSDataProvider dataProvider;

    public HandlerResolverImpl(final WSDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public List<Handler> getHandlerChain(PortInfo portInfo) {
        Object obj = dataProvider.getParameter(WSDataProvider.PARAM_SOAP_HANDLERS);
        if (!(obj instanceof List) || ((List) obj).isEmpty()) {
            List<Handler> retval = new ArrayList<Handler>(1);
            retval.add(new LoggingSoapHandler());
            return retval;
        }
        List<String> handlerClasses = (List<String>) obj;
        List<Handler> retval = new ArrayList<Handler>(handlerClasses.size());
        for (String className : handlerClasses) {
            try {
                Class cls = Utils.getWorkspaceClassLoader().loadClass(className);
                if (!Handler.class.isAssignableFrom(cls)) {
                    String errMsg = "class " + className + " does not implement " + Handler.class.getName();
                    if (Beans.isDesignTime()) {
                        MessageUtils.showErrorMessage("XML DataControl", errMsg);
                    } else {
                        throw new IllegalArgumentException(errMsg);
                    }
                }
                retval.add((Handler) cls.newInstance());
            } catch (Exception e) {
                if (Beans.isDesignTime()) {
                    MessageUtils.showErrorMessage("class load error", e.getMessage());
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
        return retval;
    }

}
