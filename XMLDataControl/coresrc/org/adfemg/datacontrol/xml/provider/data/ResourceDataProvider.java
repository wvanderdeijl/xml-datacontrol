package org.adfemg.datacontrol.xml.provider.data;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import oracle.adf.model.adapter.AdapterException;

import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * De Resource DataProvider.
 *
 * //TODO: Describe ......
 *
 * @see ProviderImpl
 * @see DataProvider
 * @see org.adfemg.datacontrol.xml.DataControl
 */
public class ResourceDataProvider extends ProviderImpl implements DataProvider {

    public static final String PARAM_RESOURCE = "resource";

    /**
     * Default no-argument constructor.
     */
    public ResourceDataProvider() {
        super();
    }

    @Override
    public Element getRootElement(final DataControlDefinitionNode dataControlDefinitionNode) {
        String resource = getParameterAsString(PARAM_RESOURCE);
        if (resource == null) {
            throw new IllegalArgumentException("parameter " + PARAM_RESOURCE + " is onbekend");
        }
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
        if (stream == null) {
            throw new IllegalArgumentException("resource \"" + resource + "\" niet gevonden");
        }
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document doc = builder.parse(stream);
            return doc.getDocumentElement();
        } catch (Exception e) {
            throw new AdapterException(e);
        }
    }
}
