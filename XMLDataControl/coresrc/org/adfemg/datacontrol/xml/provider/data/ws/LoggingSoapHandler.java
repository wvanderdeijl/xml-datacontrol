package org.adfemg.datacontrol.xml.provider.data.ws;

import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.utils.Utils;

/**
 * SOAPHandler that can log both the request and response to an ADFLogger.
 * Register this class as handler on the JAX-WS client proxy. This can be
 * done through the Handlers panel in the JAX-WS proxy editor dialog in
 * JDeveloper, with the @HandlerRegistry annotation on the javax.xml.ws.Service
 * implementation, or this handler can be registered programmatically.
 * <p>
 * By default the SOAP request and response are logged at TRACE level although
 * this default log level can be overridden with a system property named
 * <code>LoggingSoapHandler</code>. This can be used while testing from a
 * standalone JRE application. A soap-fault is always logged at WARNING level
 * and will include both the fault response as the original request payload.
 * <p>
 * Logging is done through an ADFLogger whose name is the simplified class name
 * of this handler combined with the service and operation name, for example
 * <code>org.adfemg.LoggingSoapHandler.MyTestService.divideTenOperation</code>.
 * This makes it easy to set log levels for inidividual services or operations
 * at runtime through Fusion Middleware Control.
 */
public class LoggingSoapHandler implements SOAPHandler<SOAPMessageContext> {
    // key where we keep the request SOAPMessage so we can log this in handleFault
    private static final String KEY_REQUEST = LoggingSoapHandler.class.getName() + ".Request";
    // key where we keep the start time of the request so we can measure performance
    private static final String KEY_START_TIME = LoggingSoapHandler.class.getName() + ".StartTime";

    // log level
    private static final Level LOG_LEVEL;

    // logger for internal workings of LoggingSoapHandler. All normal logging
    // is done through an ADFLogger that includes the name and operation of
    // the service
    private static final ADFLogger internalLogger = ADFLogger.createADFLogger(LoggingSoapHandler.class);

    private static final long NANOSECS_IN_MILLISECS = 1000000;

    /**
     * Since we cannot handle any header blocks in this handler, this returns
     * an empty collection.
     * @return empty collection
     */
    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }

    /**
     * The handleMessage method is invoked for normal processing of inbound and
     * outbound messages. Note that on receiving a soap-error handleError is
     * called instead. Calls #handleRequest for outbound messages or
     * #handleReponse for inbound messages.
     * @param context the message context
     * @return <code>true</code> if handler processing should continue, or
     *         <code>false</code> to block further processing. Since this
     *         handler only does logging and should be non-invasive, this always
     *         return false.
     * @see #handleRequest
     * @see #handleResponse
     */
    @Override
    public boolean handleMessage(final SOAPMessageContext context) {
        if (Boolean.TRUE.equals(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY))) {
            handleRequest(context);
        } else {
            handleResponse(context);
        }
        return true; // return true to continue processing other handlers
    }

    /**
     * Logs the payload of the request to service operation specific logger and
     * starts a performance timer by invoking ADFLogger#begin on that same
     * logger.
     * @param context the message context
     * @see #getServiceLogger
     */
    public void handleRequest(final SOAPMessageContext context) {
        // logging
        final ADFLogger logger = getServiceLogger(context);
        if (logger.isLoggable(LOG_LEVEL)) {
            logger.log(LOG_LEVEL, "invoking {0} at {1}", new Object[] {
                       fullOperationName(context), context.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY) });
            final String payload = prettyXml(context.getMessage());
            logger.log(LOG_LEVEL, "request payload\n{0}", payload);
            // start performance timer with all details (can be viewed in FMW Control)
            final HashMap<String, String> logContext = new HashMap<String, String>();
            logContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                           String.valueOf(context.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY)));
            logContext.put(MessageContext.WSDL_INTERFACE, String.valueOf(context.get(MessageContext.WSDL_INTERFACE)));
            logContext.put(MessageContext.WSDL_OPERATION, String.valueOf(context.get(MessageContext.WSDL_OPERATION)));
            logContext.put(MessageContext.WSDL_PORT, String.valueOf(context.get(MessageContext.WSDL_PORT)));
            logContext.put(MessageContext.WSDL_SERVICE, String.valueOf(context.get(MessageContext.WSDL_SERVICE)));
            logger.begin(perfTimerName(context), logContext);
        } else {
            // start performance timer even when not logging payload
            logger.begin(perfTimerName(context), new HashMap<String, String>());
        }

        // keep request for handleFault and starting time
        context.put(KEY_REQUEST, context.getMessage());
        context.put(KEY_START_TIME, System.nanoTime());
    }

    /**
     * Logs the payload of the normal response to service operation specific
     * logger and stop the performance timer that was started by handleRequest.
     * @param context the message context
     * @see #getServiceLogger
     */
    public void handleResponse(final SOAPMessageContext context) {
        // log normal response payload
        final ADFLogger logger = getServiceLogger(context);
        stopTimer(context, logger);
        if (logger.isLoggable(LOG_LEVEL)) {
            logger.log(LOG_LEVEL, "response payload\n{0}", prettyXml(context.getMessage()));
        }
    }

    /**
     * The handleFault method is invoked for fault message processing. Logs the
     * payload of the fault as well as the associated request at warning level
     * to service operation specific logger.
     * @param context the message context
     * @return <code>true</code> if handler processing should continue, or
     * <code>false</code> to block further processing. Since this
     * handler only does logging and should be non-invasive, this always
     * return false.
     * @see #getServiceLogger
     */
    @Override
    public boolean handleFault(final SOAPMessageContext context) {
        final ADFLogger logger = getServiceLogger(context);
        stopTimer(context, logger);
        // always log at warning level for SOAP Faults
        if (logger.isLoggable(ADFLogger.WARNING)) {
            logger.log(ADFLogger.WARNING, "fault response payload\n{0}\n...caused by request\n{1}", new Object[] {
                       prettyXml(context.getMessage()), prettyXml((SOAPMessage) context.get(KEY_REQUEST))
            });
        }
        return true; // return true to continue processing
    }

    /**
     * Logs the elapsed time (in milliseconds) for the service call and calls
     * ADFLogger#end to stop the performance timer that was used to show the
     * elapsed time in the log analyzer. Should only be called after
     * ADFLogger#begin was called by #handleRequest. stopTimer is called from
     * both #handleRequest and #handleFault to ensure we log the elapsed time
     * and stop the timer whether a succesfull response or fault was received.
     * @param context the message context
     * @param logger ADFLogger where messages should be logged and which has
     *               a running performance timer named by #perfTimerName that
     *               was started by invoking ADFLogger#begin
     */
    private void stopTimer(final SOAPMessageContext context, final ADFLogger logger) {
        logger.end(perfTimerName(context)); // stop ADLogger performance timer
        final long elapsed = (System.nanoTime() - (Long) context.get(KEY_START_TIME)) / NANOSECS_IN_MILLISECS;
        logger.log(LOG_LEVEL, "invoking {0} completed in {1} msecs", new Object[] {
                   fullOperationName(context), elapsed });
    }

    /**
     * Called at the conclusion of a message exchange pattern just prior to the
     * JAX-WS runtime dispatching a message, fault or exception. Performs clean
     * up of this handler and stops the performance timer that was started
     * by #handleRequest by invoking ADFLogger#end
     * @param context the message context
     * @see #getServiceLogger
     */
    @Override
    public void close(final javax.xml.ws.handler.MessageContext context) {
        // clean our state from context
        context.remove(KEY_REQUEST);
        context.remove(KEY_START_TIME);
    }

    /**
     * Gets the ADFLogger for a specific service operation. This allows for
     * separate ADFLoggers for each service operation so their logging level
     * can be set individually.
     * <p>
     * The name of the ADFLogger is the full class name of this soap handler
     * combined with the service and operation name, for example when invoking
     * the <code>divideTen</code> operation on the <code>MyTestService</code>
     * the ADFLogger will be
     * <code>org.adfemg.LoggingSoapHandler.MyTestService.divideTen</code>
     * @param context the message context that is used to determin the
     *                service and operation name
     * @return ADFLogger
     */
    public ADFLogger getServiceLogger(final MessageContext context) {
        final String loggerName =
            "org.adfemg." + LoggingSoapHandler.class.getSimpleName() + "." + fullOperationName(context);
        internalLogger.log(LOG_LEVEL, "using logger {0}", loggerName);
        final ADFLogger logger = ADFLogger.createADFLogger(loggerName);
        return logger;
    }

    /**
     * Constructs the fully qualified name of a service operation.
     * @param context the message context
     * @return service interface name and operation combined, for example
     *         <code>MyTestService.divideTen</code>
     */
    private String fullOperationName(final MessageContext context) {
        // WSDataProvider doesn't fetch WSDL so we can't use normal
        // combination of MessageContext.WSDL_INTERFACE and
        // MessageContext.WSDL_OPERATION properties. As a workaround
        // only use the service name which is set by the WSDataProvider
        return ((QName)context.get(MessageContext.WSDL_SERVICE)).getLocalPart();
    }

    /**
     * Constructs the name to use with ADFLogger#begin and ADFLogger#end. The
     * name for these two calls has to be identical and unique across other
     * timers to ensure the logging framework can link the starting and stopping
     * of the performance timer.
     * @param context the message context
     * @return <code>invoking service</code> followed by the service and
     *         operation name, for example
     *         <code>invoking service MyTestService.divideTen</code>
     * @see #fullOperationName
     */
    private String perfTimerName(final MessageContext context) {
        return "invoking service " + fullOperationName(context);
    }

    /**
     * Pretty prints a SOAPMessage to a line wrapped and indented string.
     * @param msg SOAPMessage
     * @return pretty printed XML
     */
    private String prettyXml(final SOAPMessage msg) {
        return msg == null ? null : Utils.xmlNodeToString(Utils.toXMLNode(msg.getSOAPPart().getDocumentElement()));
    }

    static {
        // initialize logging level based on system property or use default
        // of ADFLogger.TRACE (aka FINE). This makes it easy to log at higher levels in
        // a simple JRE environment without container logging
        String level = System.getProperty(LoggingSoapHandler.class.getSimpleName());
        LOG_LEVEL = level == null ? ADFLogger.TRACE : Level.parse(level);
    }
}
