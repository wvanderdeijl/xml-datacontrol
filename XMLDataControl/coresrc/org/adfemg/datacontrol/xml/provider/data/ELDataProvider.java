package org.adfemg.datacontrol.xml.provider.data;

import oracle.adf.model.adapter.AdapterContext;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.DataControl;
import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.events.DataChangeEvent;
import org.adfemg.datacontrol.xml.events.DataChangeListener;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.utils.Utils;

import org.w3c.dom.Element;


/**
 * De Expression Language DataProvider.
 *
 * Builds up an DataControl structure based on the value from an EL expression.
 * Typically used in the case of a HumanTask, to get the data from the payload of a task.
 *
 * //TODO: Describe more detailed about the use of a ELDataProvider in the DataControl.dcx.
 *
 * @see ProviderImpl
 * @see DataProvider
 * @see org.adfemg.datacontrol.xml.DataControl
 */
public class ELDataProvider extends ProviderImpl implements DataProvider {
    public static final String PARAM_ROOTELEMENT_EL_EXPR = "expression";

    private static final ADFLogger logger = AdapterContext.getDefaultContext().getLogger();

    /**
     * Default no-argument constructor.
     */
    public ELDataProvider() {
        super();
    }

    @Override
    public Element getRootElement(final DataControlDefinitionNode dataControlDefinitionNode) {
        String elExpr = getParameterAsString(PARAM_ROOTELEMENT_EL_EXPR);
        if (elExpr == null || elExpr.isEmpty()) {
            logger.warning("Set Parameter " + PARAM_ROOTELEMENT_EL_EXPR + " to a valid EL Expression.");
        }

        Object obj = ADFContext.getCurrent().getExpressionEvaluator().evaluate(elExpr);
        if (obj instanceof MutableElementWrapper) {
            return ((MutableElementWrapper) obj).getElement();
        } else {
            return Utils.toElement(obj);
        }
    }

    @Override
    public void dataControlCreated(final DataControl dc) {
        String elExpr = getParameterAsString(PARAM_ROOTELEMENT_EL_EXPR);
        if (elExpr == null || elExpr.isEmpty()) {
            logger.warning("Set Parameter " + PARAM_ROOTELEMENT_EL_EXPR + " to a valid EL Expression.");
        }
        Object obj = ADFContext.getCurrent().getExpressionEvaluator().evaluate(elExpr);
        if (obj instanceof MutableElementWrapper) {
            dc.addChangeListener(new DataControlChangeListener((MutableElementWrapper) obj));
        }
    }

    private static final class DataControlChangeListener implements DataChangeListener {
        private final MutableElementWrapper wrapper;

        public DataControlChangeListener(final MutableElementWrapper wrapper) {
            this.wrapper = wrapper;
        }

        @Override
        public void dataChanged(final DataChangeEvent event) {
            wrapper.notifyChanged();
        }
    }
}
