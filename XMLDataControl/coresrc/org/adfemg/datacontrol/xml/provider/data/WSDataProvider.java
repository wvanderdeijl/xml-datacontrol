package org.adfemg.datacontrol.xml.provider.data;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.naming.NamingException;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.soap.SOAPBinding;

import oracle.adf.model.adapter.AdapterException;
import oracle.adf.model.connection.url.URLConnection;
import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;

import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.data.XMLDCElement;
import org.adfemg.datacontrol.xml.provider.ProviderImpl;
import org.adfemg.datacontrol.xml.provider.data.ws.HandlerResolverImpl;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;
import org.adfemg.datacontrol.xml.utils.Utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;


/**
 * The WebService DataProvider.
 *
 * Builds up the request based on the configuration of the DataControl in the DataControl.dcx.
 * Calls the WebService and returns the result of the WebService.
 *
 * //TODO: Describe more detailed about the use of a WSDataProvider in the DataControl.dcx.
 *
 * @see ProviderImpl
 * @see DataProvider
 * @see org.adfemg.datacontrol.xml.DataControl
 */
public class WSDataProvider extends ProviderImpl implements DataProvider {

    private static final String NAMESPACE = "http://xmlns.adfemg.org/xmldc";

    /**
     * The service port of he WebService. Can be a fixed name as this is
     * never used in the actual http request.
     */
    public static final QName SERVICE_PORT = new QName(NAMESPACE, "XMLDCPort");

    /**
     * The name of the DataControls.dcx property for the endpoint-url of the WebService.
     */
    public static final String PARAM_END_POINT_URL = "endPointUrl";

    /**
     * The name of the DataControls.dcx property for the URL Endpoint connection of the WebService.
     */
    public static final String PARAM_END_POINT_CONNECTION = "endPointConnection";

    /**
     * The name of the DataControls.dcx property for the (optional) SOAPAction http request header.
     */
    public static final String PARAM_SOAP_ACTION_URI = "soapAction";

    /**
     * The name of the DataControls.dcx property for the (optional) soapVersion (1.1 or 1.2). When
     * not specified the more common 1.1 will be used.
     */
    public static final String PARAM_SOAP_VERSION = "soapVersion";

    /**
     * The name of the property to specify an (optional) implementation of javax.xml.ws.handler.HandlerResolver
     * which can be used to further customize the webservice request and/or response.
     */
    public static final String PARAM_HANDLER_RESOLVER = "handlerResolverClass";

    /**
     * The name of the DataControls.dcx property for the SOAP Body Request Element for the WebService call.
     */
    public static final String PARAM_REQUEST_ELEM = "requestElement";

    /**
     * The name of the DataControls.dcx property for the (optional) SOAP Header Element for the WebService call.
     */
    public static final String PARAM_HEADER_ELEM = "headerElement";

    /**
     * The name of the DataControls.dcx property for the list of SoapHandlers.
     */
    public static final String PARAM_SOAP_HANDLERS = "soapHandlers";

    /**
     * The ADFLogger.
     */
    private static final ADFLogger logger = ADFLogger.createADFLogger(WSDataProvider.class);

    /**
     * RegEx to strip the parameter from the Text Nodes.
     */
    private static final Pattern REPLACE_PATTERN = Pattern.compile("(.*?)(\\#\\{param\\.)([^}]*)(\\})(.*)", Pattern.DOTALL);

    /**
     * The number corresponding with the REPLACE_PATTERN group for the prefix.
     */
    private static final int RP_PREFIX = 1;

    /**
     * The number corresponding with the REPLACE_PATTERN group for the paramName.
     */
    private static final int RP_PARAM_NAME = 3;

    /**
     * The number corresponding with the REPLACE_PATTERN group for the postfix.
     */
    private static final int RP_POST_FIX = 5;

    /**
     * Default no-argument constructor.
     */
    public WSDataProvider() {
        super();
    }

    /**
     * Append the SOAPBody with the Request.
     * Throws an error when the requestElement from the DataControl is incorrect.
     *
     * @param soapBody the SOAPBody.
     * @param typeMapper The typeMapper for translation between XML and Java Objects.
     */
    protected void appendSOAPBody(final SOAPBody soapBody, final TypeMapper typeMapper) throws SOAPException {
        Node n = (Node) getParameter(PARAM_REQUEST_ELEM);
        if (n == null) {
            throw new IllegalArgumentException(PARAM_REQUEST_ELEM + " argument is unknown or empty.");
        }
        // copy node from DataControls.dcx to soapBody
        Node requestNode = soapBody.getOwnerDocument().importNode(n, true);
        soapBody.appendChild(requestNode);
        replaceParameters(requestNode, typeMapper); // replace any #{param.xxx} placeholders
    }

    /**
     * Append the SOAPHeader with the Request.
     *
     * @param soapHeader the SOAPHeader.
     * @param typeMapper The typeMapper for translation between XML and Java Objects.
     */
    protected void appendSOAPHeader(final SOAPHeader soapHeader, final TypeMapper typeMapper) throws SOAPException {
        Node n = (Node) getParameter(PARAM_HEADER_ELEM);
        if (n == null) {
            return;
        }
        // copy node from DataControls.dcx to soapHeader
        Node headerNode = soapHeader.getOwnerDocument().importNode(n, true);
        soapHeader.appendChild(headerNode);
        replaceParameters(headerNode, typeMapper); // replace any #{param.xxx} placeholders
    }

    /**
     * Replace any #{param.xxx} expressions with their actual value in all TextNode
     * descendents of the given Node.
     * <p>
     * Any Text node that contains a #{param.xxx} will be split into three separate
     * Text nodes: the text before the expression, the value of the expression itself
     * and the text after the expression.
     * <p>
     * This method will invoke itself recursively for all children of the given node so
     * all descendents are processed.
     * @param node the Node to start searching.
     * @param typeMapper The typeMapper for translation between XML and Java Objects.
     */
    // TODO: use typeMapper
    protected void replaceParameters(final Node node, final TypeMapper typeMapper) {
        if (node instanceof Text) {
            String text = node.getNodeValue();
            Matcher matcher = REPLACE_PATTERN.matcher(text);
            if (matcher.matches()) {
                Document document = node.getOwnerDocument();
                Node parent = node.getParentNode();

                String prefix = matcher.group(RP_PREFIX);
                String paramName = matcher.group(RP_PARAM_NAME);
                String postfix = matcher.group(RP_POST_FIX);
                if (!prefix.isEmpty()) {
                    Text prefixNode = document.createTextNode(prefix);
                    parent.insertBefore(prefixNode, node);
                }
                Object paramVal = getParameter(paramName);
                if (paramVal instanceof XMLDCElement) {
                    //Object, translate to Node.
                    XMLDCElement dcElem = (XMLDCElement) paramVal;
                    Element elem = dcElem.getElement();
                    Node importedNode = document.importNode(elem, true);
                    parent.insertBefore(importedNode, node);
                } else if (paramVal instanceof Node) {
                    // import XML Node.
                    Node importedNode = document.importNode((Node) paramVal, true);
                    parent.insertBefore(importedNode, node);
                } else if (paramVal instanceof NodeList) {
                    // import NodeList
                    NodeList nl = (NodeList) paramVal;
                    for (int i = 0, num = nl.getLength(); i < num; i++) {
                        Node importedNode = document.importNode(nl.item(i), true);
                        parent.insertBefore(importedNode, node);
                    }
                } else if (paramVal != null) {
                    //Not an object, but a simpleType.
                    // TODO: shouldn't we use the TypeMapper ?
                    Text valueNode = document.createTextNode(paramVal.toString());
                    parent.insertBefore(valueNode, node);
                }
                if (!postfix.isEmpty()) {
                    Text postfixNode = document.createTextNode(postfix);
                    parent.insertBefore(postfixNode, node);
                    //Call the replace again, this time with the post part, the pattern can be in here once more.
                    replaceParameters(postfixNode, typeMapper);
                }
                //Remove the node we just split up.
                parent.removeChild(node);
            }
        }
        // traverse all children
        NodeList childNodes = node.getChildNodes();
        if (childNodes == null) {
            return;
        }
        for (int i = 0; i < childNodes.getLength(); i++) { // recursive call for all children
            replaceParameters(childNodes.item(i), typeMapper);
        }
    }

    /**
     * Get the endPointUrl from the parameter on the DataControl.
     * @return the endPointUrl.
     */
    protected String getEndPointUrl() {
        return getParameterAsString(PARAM_END_POINT_URL);
    }

    /**
     * Get the soapAction from the parameter on the DataControl.
     * @return the soapActionUri.
     */
    protected String getSoapActionUri() {
        return getParameterAsString(PARAM_SOAP_ACTION_URI);
    }

    /**
     * Get the soap binding identifier (namespace) based on the soapVersion parameter on the DataControl.
     * @return {@link SOAPBinding#SOAP12HTTP_BINDING} if the soapVersion parameter in the
     *         DataControl is 1.2 or {@link SOAPBinding#SOAP11HTTP_BINDING} in any other situation
     *         as that is the more prevelant version
     */
    protected String getSoapBinding() {
        return "1.2".equals(getParameterAsString(PARAM_SOAP_VERSION)) ? SOAPBinding.SOAP12HTTP_BINDING :
               SOAPBinding.SOAP11HTTP_BINDING;
    }

    /**
     * Get the endPointConnection from the parameter on the DataControl.
     * @return instance of URLConnection or null if none specified or found.
     */
    protected URLConnection getEndPointConnection() {
        String connName = getParameterAsString(PARAM_END_POINT_CONNECTION);
        if (connName == null || connName.isEmpty()) {
            return null;
        }
        try {
            Context ctx = ADFContext.getCurrent().getConnectionsContext();
            Object obj = ctx.lookup(connName);
            if (obj instanceof URLConnection) {
                return (URLConnection) obj;
            }
            return null;
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Composes a QName from the ServiceName and ServiceNameSpace on the DataControl.
     * This is a fixed QName as it is not used in the actual http request.
     * @return QName of the service.
     */
    protected QName getServiceName(final DataControlDefinitionNode dataControlDefinitionNode) {
        // use dataControl name as service name as this is used by the LoggingSoapHandler
        return new QName(NAMESPACE, dataControlDefinitionNode.getDataControlDefinition().getName());
    }

    /**
     * Compose a QName based on the ServicePort and the ServicePortNamespace on the DataControl.
     * This is a fixed QName as it is not used in the actual http request.
     * @return QName of the service.
     */
    protected QName getPortName() {
        return SERVICE_PORT;
    }

    protected HandlerResolver getHandlerResolver() {
        String resolver = getParameterAsString(PARAM_HANDLER_RESOLVER);
        if (resolver == null || resolver.isEmpty()) {
            return new HandlerResolverImpl(this);
        }
        try {
            Class cls = Utils.getWorkspaceClassLoader().loadClass(resolver);
            return (HandlerResolver) cls.newInstance();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Delivers the XML Element to the DataControl.
     *
     * Creates the Service based on the parameters set on the DataControl.
     * Call the Service and return the firstChild.
     *
     * @param dataControlDefinitionNode The dataControlDefinition for which the service is called.
     * @return The RootElement as an org.w3c.dom.Element.
     */
    @Override
    public Element getRootElement(final DataControlDefinitionNode dataControlDefinitionNode) {
        // TODO: refactor this method to use many separate protected methods so subclasses can override this
        Element retval;

        // get Service Information from datacontrol configuration
        QName servName = getServiceName(dataControlDefinitionNode);
        QName portName = getPortName();
        String endPointUrl = getEndPointUrl();
        String soapActionUri = getSoapActionUri();
        String soapBindingId = getSoapBinding();
        URLConnection connection = null;
        if (endPointUrl == null || endPointUrl.isEmpty()) {
            connection = getEndPointConnection();
            if (connection != null) {
                endPointUrl = connection.getURL().toExternalForm();
            }
        }

        // create service.
        Service service = Service.create(servName);
        service.addPort(portName, soapBindingId, endPointUrl);

        // register (optional) HandlerResolver with the service so users can further customize the
        // request (and response)
        // TODO: default HandlerResolver that gets list of classNames from WSDataProvider list-parameter
        // TODO: default implementation should return single LoggingSoapHandler, but users could add their
        //       own to perhaps fill default information in SoapHeader
        HandlerResolver hr = getHandlerResolver();
        if (hr != null) {
            service.setHandlerResolver(hr);
        }

        // create a Dispatch instance from a service
        Dispatch<SOAPMessage> dispatch = service.createDispatch(portName, SOAPMessage.class, Service.Mode.MESSAGE);
        if (soapActionUri != null && !soapActionUri.isEmpty()) {
            dispatch.getRequestContext().put(BindingProvider.SOAPACTION_USE_PROPERTY, Boolean.TRUE);
            dispatch.getRequestContext().put(BindingProvider.SOAPACTION_URI_PROPERTY, soapActionUri);
        }

        // compose a request message of a SOAPHeader and SOAPBody
        try {
            MessageFactory mf = ((SOAPBinding) dispatch.getBinding()).getMessageFactory();

            SOAPMessage request = mf.createMessage();
            appendSOAPHeader(request.getSOAPHeader(), dataControlDefinitionNode.getProviderInstance(TypeMapper.class));
            appendSOAPBody(request.getSOAPBody(), dataControlDefinitionNode.getProviderInstance(TypeMapper.class));

            // Invoke the service endpoint.
            if (logger.isConfig()) {
                HashMap<String, String> context = new HashMap<String, String>();
                context.put("serviceName", servName.toString());
                context.put("portName", portName.toString());
                context.put("endPointUrl", endPointUrl);
                logger.begin("invoking web service", context);
            }
            try {
                SOAPMessage response = dispatch.invoke(request);
                SOAPBody respBody = response.getSOAPBody();
                // find first child Element of SOAPBody
                NodeList children = respBody.getChildNodes();
                retval = null;
                if (children != null) {
                    for (int i = 0; i < children.getLength(); i++) {
                        if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
                            retval = (Element) children.item(i);
                            break;
                        }
                    }
                }
            } finally {
                logger.end("invoking web service");
            }
        } catch (SOAPException e) {
            logger.severe(e);
            throw new AdapterException(e);
        }
        return retval;
    }

}
