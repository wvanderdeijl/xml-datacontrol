package org.adfemg.datacontrol.xml.provider.data;

import org.adfemg.datacontrol.xml.DataControlDefinitionNode;
import org.adfemg.datacontrol.xml.provider.Provider;

import org.w3c.dom.Element;


/**
 * The Interface for a provider of XML data for a DataControl.
 *
 * @see Provider
 */
public interface DataProvider extends Provider {

    /**
     * Provides the XML Element to the DataControl.
     *
     * @param dataControlDefinitionNode The dataControlDefinitionNode for which the element is provided.
     *                                  This is not allowed to be <code>null</code>.
     * @return The rootElement as an org.w3c.dom.Element.
     */
    Element getRootElement(DataControlDefinitionNode dataControlDefinitionNode);
}
