package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.adf.model.utils.JSR227Util;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.ArrayListDefinitionContainer;
import oracle.binding.meta.DefinitionContainer;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class CollectionOperations extends AbstractOperationsDefinition {

    DefinitionContainer operations;

    public CollectionOperations(AccessorDefinition parent, TypeMapper typeMapper) {
        super(parent, parent.getFullName() + ".CollectionOperations", typeMapper);
    }

    @Override
    public DefinitionContainer getOperationDefinitions() {
        if (operations == null) {
            ArrayListDefinitionContainer ops = new ArrayListDefinitionContainer();
            if (!((AccessorDefinition) getDefinitionParent()).isReadOnly()) {
                ops.add(JSR227Util.createCreateAction());
                ops.add(JSR227Util.createRemoveAction());
            }
            ops.add(JSR227Util.createFirstAction());
            ops.add(JSR227Util.createLastAction());
            ops.add(JSR227Util.createPreviousAction());
            ops.add(JSR227Util.createNextAction());
            this.operations = ops;
        }
        return operations;
    }

}
