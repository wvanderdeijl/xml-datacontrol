package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.adf.model.utils.JSR227Util;

import oracle.binding.meta.AccessorDefinition;
import oracle.binding.meta.ArrayListDefinitionContainer;
import oracle.binding.meta.DefinitionContainer;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class ScalarOperations extends AbstractOperationsDefinition {

    DefinitionContainer operations;

    public ScalarOperations(AccessorDefinition parent, TypeMapper typeMapper) {
        super(parent, parent.getFullName() + ".Operations", typeMapper);
    }

    @Override
    public DefinitionContainer getOperationDefinitions() {
        if (operations == null) {
            ArrayListDefinitionContainer ops = new ArrayListDefinitionContainer();
            if (!((AccessorDefinition) getDefinitionParent()).isReadOnly()) {
                ops.add(JSR227Util.createCreateAction());
                ops.add(JSR227Util.createRemoveAction());
            }
            this.operations = ops;
        }
        return operations;
    }

}
