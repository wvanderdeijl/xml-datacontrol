package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.AttributeDefinition;
import oracle.binding.meta.Definition;
import oracle.binding.meta.NamedDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public abstract class AbstractAttributeDefinition extends AbstractNamedDefinition implements AttributeDefinition {

    private final String simpleName;

    public AbstractAttributeDefinition(NamedDefinition parent, String simpleName, TypeMapper typeMapper) {
        super(parent, typeMapper);
        this.simpleName = simpleName;
    }

    @Override
    public String getFullName() {
        Definition parent = getDefinitionParent();
        if (!(parent instanceof NamedDefinition)) {
            throw new IllegalStateException("getFullName requires parent to be a NamedDefinition");
        }
        return ((NamedDefinition) parent).getFullName() + "." + getName();
    }

    @Override
    public String getName() {
        return simpleName;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public boolean isKey() {
        return false;
    }

    @Override
    public String getSourceTypeString() {
        return getJavaTypeString();
    }

    @Override
    public int getDefinitionType() {
        return Definition.TYPE_ATTRIBUTE;
    }

}
