package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.NamedDefinition;

import oracle.xml.parser.schema.XSDAttribute;
import oracle.xml.parser.schema.XSDSimpleType;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class AttributeAdapter extends AbstractAttributeDefinition {

    private final XSDAttribute attribute;

    public AttributeAdapter(NamedDefinition parent, String simpleName, TypeMapper typeMapper, XSDAttribute attribute) {
        super(parent, simpleName, typeMapper);
        this.attribute = attribute;
    }

    @Override
    public String getJavaTypeString() {
        return getTypeMapper().getJavaType((XSDSimpleType) attribute.getType());
    }

}
