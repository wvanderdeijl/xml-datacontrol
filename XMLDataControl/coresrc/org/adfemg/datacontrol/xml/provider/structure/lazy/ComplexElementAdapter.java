package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.NamedDefinition;
import oracle.binding.meta.StructureDefinition;

import oracle.xml.parser.schema.XSDComplexType;
import oracle.xml.parser.schema.XSDElement;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public class ComplexElementAdapter extends AbstractAccessorDefinition {

    private final XSDElement element; // constructor argument

    private final boolean collection;
    private StructureDefinition structDef;

    public ComplexElementAdapter(NamedDefinition parent, String simpleName, TypeMapper typeMapper, XSDElement element) {
        super(parent, simpleName, typeMapper);
        this.element = element;
        this.collection = element.getMaxOccurs() > 1;
    }

    @Override
    public boolean isCollection() {
        return collection;
    }

    @Override
    public StructureDefinition getStructure() {
        if (structDef == null) {
            structDef =
                new ComplexTypeAdapter(this, getFullName(), getTypeMapper(), (XSDComplexType) element.getType());
        }
        return structDef;
    }

}
