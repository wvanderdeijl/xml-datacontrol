package org.adfemg.datacontrol.xml.provider.structure.lazy;

import oracle.binding.meta.Definition;
import oracle.binding.meta.NamedDefinition;
import oracle.binding.meta.StructureDefinition;

import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

public abstract class AbstractStructureDefinition extends AbstractNamedDefinition implements StructureDefinition {

    private String fullName;

    public AbstractStructureDefinition(NamedDefinition parent, String fullName, TypeMapper typeMapper) {
        super(parent, typeMapper);
        this.fullName = fullName;
    }

    @Override
    public String getName() {
        return fullName.substring(fullName.lastIndexOf('.'));
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public int getDefinitionType() {
        return Definition.TYPE_STRUCTURE;
    }

}
