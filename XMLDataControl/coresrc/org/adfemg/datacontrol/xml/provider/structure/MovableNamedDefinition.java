package org.adfemg.datacontrol.xml.provider.structure;

import oracle.binding.meta.Definition;
import oracle.binding.meta.NamedDefinition;

public interface MovableNamedDefinition extends NamedDefinition {
    void setParent(Definition parent);
}
