package org.adfemg.datacontrol.xml.provider.structure;

import oracle.xml.parser.schema.XSDNode;

import org.adfemg.datacontrol.xml.provider.Provider;
import org.adfemg.datacontrol.xml.provider.typemap.TypeMapper;

/**
 * The Interface for a provider of the datacontrol structure for a DataControl.
 *
 * @see Provider
 */
public interface StructureProvider extends Provider {

    /**
     * Builds the structure for a definition node in the DataControls.dcx file. Typically adds a
     * OperationDefinition to the given root structure including its return type and all
     * nested structures, accessors, attributes, etc.
     * @param node the XSDElement or XSDComplexType to create the structure for. Implementors only need to be
     *             able to accept these two subclasses of XSDNode.
     * @param structName the name of the root StructureDef to create. The implementation is free to create names
     *                   for any nested accessors, structures, etc but it should prefix the names of all these
     *                   nested structures with this name to ensure global uniqueness.
     * @return StructureDef the created structure. It is the caller's responsibility to set/change the parent of the
     *                      returned structure.
     */
    MovableStructureDefinition buildStructure(XSDNode node, String structName, TypeMapper typeMapper);

}
