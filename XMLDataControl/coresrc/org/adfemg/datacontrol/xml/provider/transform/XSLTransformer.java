package org.adfemg.datacontrol.xml.provider.transform;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XSLTransformer extends ProviderImpl implements TransformationProvider {

    public static final String PARAM_STYLESHEET = "stylesheet";

    private static final TransformerFactory transformerFactory = TransformerFactory.newInstance();

    private static final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder docBuilder;

    /**
     * Default no-argument constructor.
     */
    public XSLTransformer() {
        super();
    }

    @Override
    public Element transform(Element input) {
        DOMResult out = new DOMResult();
        transform(input, out);
        if (!(out.getNode() instanceof Document)) {
            throw new IllegalStateException("XSL transformation did not return a Document");
        }
        return ((Document) out.getNode()).getDocumentElement();
    }

    protected Source getStylesheet() {
        String stylesheet = getParameterAsString(PARAM_STYLESHEET);
        if (stylesheet == null) {
            throw new IllegalArgumentException("parameter " + PARAM_STYLESHEET + " unknown");
        }
        InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(stylesheet);
        if (stream == null) {
            throw new IllegalArgumentException("stylesheet \"" + stylesheet + "\" not found as resource");
        }
        return new StreamSource(stream);
    }

    protected void transform(Element input, Result dest) {
        try {
            Transformer transformer = transformerFactory.newTransformer(getStylesheet());
            Document inputDoc = createDocument(input); // create document for XSL transformation
            Source in = new DOMSource(inputDoc);
            transformer.transform(in, dest);
        } catch (TransformerConfigurationException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    protected synchronized Document createDocument(Element rootElement) throws ParserConfigurationException {
        if (docBuilder == null) {
            docBuilder = docBuilderFactory.newDocumentBuilder();
        }
        Document doc = docBuilder.newDocument();
        Node importedNode = doc.importNode(rootElement, true);
        doc.appendChild(importedNode);
        return doc;
    }

}
