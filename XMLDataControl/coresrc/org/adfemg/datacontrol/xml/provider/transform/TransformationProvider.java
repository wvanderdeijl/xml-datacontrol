package org.adfemg.datacontrol.xml.provider.transform;

import org.adfemg.datacontrol.xml.provider.Provider;

import org.w3c.dom.Element;

// TODO: wouldn't it be better to rewrite this to something like a DataProviderFilter. It would "wrap" a
//       DataProvider (or another DataProviderFilter) and would be able to manipulate the request (dynamic
//       parameters) going to the wrapped provider as well as manipulate the response from the wrapped
//       provider. This would allow for a chaing of DataProviderFilter's that can do things like XSL transforms,
//       logging, validation, injecting of system derived dynamic parameters, etc.
//       It should also allow the DataProviderFilter's to have dynamic parameters, so for example the path
//       to the XSL for a XSLFilter could be dynamic and supplied from the binding layer.
public interface TransformationProvider extends Provider {

    Element transform(Element input);

}
