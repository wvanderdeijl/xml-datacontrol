package org.adfemg.datacontrol.xml.provider.transform;

import org.adfemg.datacontrol.xml.provider.ProviderImpl;

import org.w3c.dom.Element;

public class NullTransformationProvider extends ProviderImpl implements TransformationProvider {

    @Override
    public Element transform(Element input) {
        return input;
    }

}
