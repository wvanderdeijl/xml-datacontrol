package org.adfemg.common.events;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.lang.reflect.Array;

import java.util.ArrayList;
import java.util.List;

import org.adfemg.common.reflect.BroadcastProxy;


/**
 * This class offers support for registering Listeners and notify listeners of changes.
 *
 * Input argument to the constructor has to be a Listener interface.
 * To notify a listener of an event the method <code> {@link #broadcast()}</code>
 * can be called on the interface.
 * <p>
 * This class is thread-safe.
 *
 * @param <T> the Listener Interface
 */
@SuppressWarnings("PMD.AvoidUsingVolatile")
// There are some variables marked with volatile. This is needed for thread-safety.
public class ListenerSupport<T> implements Serializable {
    @SuppressWarnings("compatibility:-1530203360724239667")
    private static final long serialVersionUID = 1L;

    private transient volatile List<T> listeners;

    private transient volatile T proxy;

    private final Class<T> cls;

    /**
     * The only constructor for <code>ListenerSupport</code>.
     *
     * @param cls the Listener Interface
     */
    public ListenerSupport(final Class<T> cls) {
        if (!cls.isInterface()) {
            throw new IllegalArgumentException("ListenerSupport can only be used with an interface.");
        }

        this.cls = cls;
    }

    /**
     * Only allowed to be called with a lock.
     *
     * @return Listeners field, garantied that it is not-null.
     */
    private List<T> ensureListeners() {
        if (listeners == null) {
            listeners = new ArrayList<T>();
        }
        return listeners;
    }

    /**
     * Register a <code>Listener</code>.
     * We do not check for duplicates. To prevent duplicates, use
     * <code>{@link #removeListener}</code> first.
     *
     * @param listener the Listener to add.
     */
    public void addListener(final T listener) {
        synchronized (this) {
            ensureListeners().add(listener);
        }
    }

    /**
     * Removes a registered <code>listener</code>.
     * Doesn't throw an error if the <code>listener</code> was not yet registered.
     *
     * @param listener the Listener to be removed.
     */
    public void removeListener(final T listener) {
        synchronized (this) {
            if (listeners != null) {
                listeners.remove(listener);
            }
        }
    }

    /**
     * Remove all registered listeners and cached proxy.
     */
    @SuppressWarnings("PMD.NullAssignment")
    public void clear() {
        synchronized (this) {
            listeners = null;
            proxy = null;
        }
    }

    /**
     * Gets a copy of the registered listeners list.
     *
     * @return an array with the registered listeners.
     */
    public T[] getListeners() {
        synchronized (this) {
            if (listeners != null) {
                return listeners.toArray((T[]) Array.newInstance(cls, listeners.size()));
            }
        }
        return (T[]) Array.newInstance(cls, 0);
    }

    /**
     * Gives an implementation off the listener interface.
     *
     * This implementation "forward" the methods to the collection of listeners.
     * So every methode that is called on the on the broadcast proxy, will also
     * be called on the listeners.
     *
     * @return a <code>BroadcastProxy</code> that implements the interface <code>T</code>.
     * @see BroadcastProxy
     */
    public T broadcast() {
        synchronized (this) {
            if (proxy == null) {
                proxy = BroadcastProxy.create(cls, ensureListeners());
            }
            return proxy;
        }
    }

    /**
     * At serialization time we skip non-serializable listeners and
     * only serialize the serializable listeners.
     *
     * @param s the ObjecOutputStream
     * @throws IOException
     * @serialData Null terminated list of listeners
     */
    private void writeObject(final ObjectOutputStream s) throws IOException {
        s.defaultWriteObject();

        if (listeners != null) {
            for (Object listener : listeners.toArray()) {
                if (listener instanceof Serializable) {
                    s.writeObject(listener);
                }
            }
        }
        s.writeObject(null);
    }


    /**
     * Deserialization.
     *
     * @param s the ObjectInputStream
     * @throws ClassNotFoundException
     * @throws IOException
     */
    private void readObject(final ObjectInputStream s) throws ClassNotFoundException, IOException {
        s.defaultReadObject();

        for (Object listener = s.readObject(); listener != null; listener = s.readObject()) {
            addListener(cls.cast(listener));
        }
    }
}
