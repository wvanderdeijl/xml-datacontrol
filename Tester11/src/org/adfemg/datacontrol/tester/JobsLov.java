package org.adfemg.datacontrol.tester;

import oracle.jbo.ViewObject;

import org.adfemg.datacontrol.lov.ConjunctionCriterionImpl;
import org.adfemg.datacontrol.lov.LovBean;
import org.adfemg.datacontrol.lov.ModelImpl;
import org.adfemg.datacontrol.lov.QueryDescriptorImpl;
import org.adfemg.datacontrol.lov.QueryModelImpl;


public class JobsLov extends LovBean
{
  @Override
  protected ModelImpl createModel()
  {
    ModelImpl retval = super.createModel();
    // aangeven welke kolommen uit de LOV moeten worden "teruggeschreven" naar attribute-bindings in de pagina
    // een optioneel derde argument true geeft aan dat deze mapping ook gebruikt moet worden bij het opzoeken
    // van de waarde in de lijst van LOV waarden (de zogenaamde key attribute(s))
    retval.mapColumnToBinding("id", "#{row.Job.bindings.id.inputValue}", true);
    return retval;
  }

//  @Override
//  protected TableModelImpl createTableModel(ViewObject vo)
//  {
//    TableModelImpl tab = super.createTableModel(vo);
//    // standaard kolom volgorde was niet akkoord, dus aanpassen
//    tab.orderColumns("id", "title");
//    return tab;
//  }

  @Override
  protected QueryDescriptorImpl createQueryDescriptor(QueryModelImpl qmodel,
                                                      ConjunctionCriterionImpl crits,
                                                      ViewObject viewObject)
  {
    QueryDescriptorImpl retval = super.createQueryDescriptor(qmodel, crits, viewObject);
    // standaard wordt eerste kolom (code) gebruikt voor auto-complete terwijl
    // wij op de omschrijving willen zoeken
    retval.setAutoCompleteCriterion("title");
    return retval;
  }
}
