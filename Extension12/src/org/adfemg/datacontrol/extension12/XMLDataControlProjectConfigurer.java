package org.adfemg.datacontrol.extension12;

import oracle.adf.model.adapter.AbstractDefinition;

import oracle.adfdt.model.ide.adapter.AbstractDataControlProjectConfigurer;

import oracle.ide.model.Project;

import oracle.jdeveloper.model.JProjectLibraries;

import org.adfemg.datacontrol.xml.DataControlDefinition;

/**
 * Invoked at design time when a new XML Data Control is created using the New Gallery
 * wizard. When creating a DC at the end of this wizard each registered
 * DataControlProjectConfigurer gets a change to configure the project for the DC
 * being created. This implementation only responds to our XML Data Control being
 * created and will add our XML Data Control java library (and other required
 * libraries) to the project.
 * <p>Registered in <code>extension.xml</code> as <code>data-control-configurer-hook</code>
 * <p>inspired by oracle.adfdtinternal.model.adapter.jmx.JMXDataControlProjectConfigurer
 */
public class XMLDataControlProjectConfigurer extends AbstractDataControlProjectConfigurer {

    @Override
    public boolean applyProjectConfiguration(Project project, AbstractDefinition dcDefinition) {
        return dcDefinition instanceof DataControlDefinition;
    }

    @Override
    public void configureDCProject(Project project) {
        super.configureDCProject(project); // adds ADF Model and XML Parser libs
        JProjectLibraries projLibs = JProjectLibraries.getInstance(project);
        // TODO: get name from resource bundle (and use same bundle in
        // naming the library in extension.xml)
        projLibs.addLibrary("ADFEMG.org XML Data Control");
    }

}
