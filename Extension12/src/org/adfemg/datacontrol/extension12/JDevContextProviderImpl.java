package org.adfemg.datacontrol.extension12;

import java.util.HashMap;

import oracle.adf.model.adapter.AbstractDefinition;

import oracle.adfdtinternal.model.ide.adapter.JDevContextProvider;

import oracle.bali.xml.util.ContextualActionProvider;

import org.w3c.dom.Node;

public class JDevContextProviderImpl extends JDevContextProvider {

    public JDevContextProviderImpl() {
        super();
    }

    @Override
    public ContextualActionProvider getContextualActionProvider(String string, Node node,
                                                                AbstractDefinition abstractDefinition) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getTooltipText(String string, Node node, AbstractDefinition abstractDefinition) {
        // TODO Implement this method
        if (abstractDefinition != null) {
            abstractDefinition.loadFromMetadata(node, new HashMap());
        }
        return super.getTooltipText(string, node, abstractDefinition);
    }
}
