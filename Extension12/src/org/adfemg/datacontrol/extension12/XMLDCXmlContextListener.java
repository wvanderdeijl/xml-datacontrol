package org.adfemg.datacontrol.extension12;

import oracle.adfdt.model.ide.navigator.nodes.DataControlConfigurationNode;

import oracle.bali.xml.gui.jdev.JDevXmlContext;
import oracle.bali.xml.model.XmlContext;
import oracle.bali.xml.model.XmlContextSetupHook;

public class XMLDCXmlContextListener implements XmlContextSetupHook {

    public XMLDCXmlContextListener() {
        super();
    }

    @Override
    public void setup(XmlContext context) {
        //        if (!(context instanceof JDevXmlContext)) {
        //              return;
        //            }
        //
        //            if (!(((JDevXmlContext)context).getIdeDocument() instanceof DataControlConfigurationNode))
        //            {
        //              return;
        //            }

        context.getModel().addModelListener(new XMLDCModelListener());
    }
}
