package org.adfemg.datacontrol.lov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.ConjunctionCriterion;
import oracle.adf.view.rich.model.Criterion;
import oracle.adf.view.rich.model.QueryDescriptor;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaComponent;
import oracle.jbo.ViewObject;


public class ConjunctionCriterionImpl extends ConjunctionCriterion implements QueryModeHandler {

    private ConjunctionCriterion.Conjunction conjunction = ConjunctionCriterion.Conjunction.AND;
    private final List<Criterion> criteria = new ArrayList<Criterion>();

    private QueryDescriptor.QueryMode mode;

    /**
     * No-arg default constructor.
     */
    public ConjunctionCriterionImpl() {
        super();
    }

    public ConjunctionCriterionImpl(final List<Criterion> criteria) {
        for (Criterion crit : criteria) {
            this.addCriterion(crit);
        }
    }

    @Override
    public ConjunctionCriterion.Conjunction getConjunction() {
        return conjunction;
    }

    @Override
    public List<Criterion> getCriterionList() {
        // TODO: In basic mode hide the custom added criteria.
        // TODO: Test wether the key is still correct after this.
        return Collections.unmodifiableList(criteria);
    }

    public final void addCriterion(final Criterion criterion) {
        if (!(criterion instanceof AttributeCriterion)) {
            throw new UnsupportedOperationException("ConjunctionCriterionImpl.addCriterion can only add an AttributeCriterion.");
        }
        final AttributeCriterion attrCrit = (AttributeCriterion) criterion;
        boolean found = false;
        for (int i = 0; i < criteria.size(); i++) {
            final Criterion iterCrit = criteria.get(i);
            if (!(iterCrit instanceof AttributeCriterion)) {
                continue;
            }
            final AttributeCriterion iterAttrCrit = (AttributeCriterion) iterCrit;
            if (found && !iterAttrCrit.getAttribute().equals(attrCrit.getAttribute())) {
                // First criterion found for other attribute than the to add attr.
                criteria.add(i, criterion);
                return;
            } else {
                found = found || iterAttrCrit.getAttribute().equals(attrCrit.getAttribute());
            }
        }
        // No next criterion found, so add at the end of the List.
        criteria.add(criterion);
    }

    public void removeCriterion(final Criterion criterion) {
        criteria.remove(criterion);
    }


    /**
     * This is also used as key in the EL Expression
     * lovModel.queryDescriptor.conjunctionCriterion.criterionList[0]
     * That's why we return an index as Integer.
     *
     * @param criterion
     * @return the index.
     */
    @Override
    public Object getKey(final Criterion criterion) {
        final int index = criteria.indexOf(criterion);
        if (index == -1) {
            throw new IllegalArgumentException("Criterion not found. ");
        }
        return String.valueOf(index);
    }

    @Override
    public Criterion getCriterion(final Object key) {
        final int index = Integer.parseInt((String) key);
        return criteria.get(index);
    }

    @Override
    public void setConjunction(final ConjunctionCriterion.Conjunction conjunction) {
        this.conjunction = conjunction;
    }

    public void conjunctNone() {
        setConjunction(ConjunctionCriterion.Conjunction.NONE);
    }

    public void conjunctAnd() {
        setConjunction(ConjunctionCriterion.Conjunction.AND);
    }

    public void conjunctOr() {
        setConjunction(ConjunctionCriterion.Conjunction.OR);
    }

    @Override
    public void changeMode(final QueryDescriptor.QueryMode mode) {
        this.mode = mode;
        for (Criterion crit : criteria) {
            if (crit instanceof QueryModeHandler) {
                ((QueryModeHandler) crit).changeMode(mode);
            }
        }
    }

    @Override
    public QueryDescriptor.QueryMode getMode() {
        return mode;
    }

    public ViewCriteria toViewCriteria(final ViewObject vo) {
        final ViewCriteria retval = vo.createViewCriteria();
        retval.setCriteriaMode(ViewCriteria.CRITERIA_MODE_CACHE);
        retval.setConjunction(ConjunctionCriterion.Conjunction.OR == getConjunction() ?
                              ViewCriteriaComponent.VC_CONJ_OR : ViewCriteriaComponent.VC_CONJ_AND);
        for (Criterion crit : criteria) {
            if (crit instanceof AttributeCriterionImpl) {
                final AttributeCriterionImpl attrCrit = (AttributeCriterionImpl) crit;
                attrCrit.addViewCriteriaRow(retval, getConjunction());
            } else {
                throw new UnsupportedOperationException("ConjunctionCriterionImpl.toViewCriteria does not support " +
                                                        crit.getClass().getName());
            }
        }
        return retval;
    }

    public void reset() {
        conjunction = ConjunctionCriterion.Conjunction.OR;
        final Iterator<Criterion> iterator = criteria.iterator();
        while (iterator.hasNext()) {
            final Criterion crit = iterator.next();
            if (crit instanceof AttributeCriterion) {
                final AttributeCriterion ac = (AttributeCriterion) crit;
                if (ac.isRemovable()) {
                    iterator.remove();
                } else {
                    final List values = ac.getValues();
                    for (int i = 0; i < values.size(); i++) {
                        values.set(i, null);
                    }
                }
            }
        }
    }
}
