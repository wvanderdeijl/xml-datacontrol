package org.adfemg.datacontrol.lov;

import java.util.AbstractMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import oracle.jbo.Row;


public class RowMapAdapter extends AbstractMap<String, Object> {

    private final Row row;
    private Set<Map.Entry<String, Object>> entries = null;

    public RowMapAdapter(final Row row) {
        this.row = row;
    }

    public Set<Map.Entry<String, Object>> entrySet() {
        if (entries == null) {
            if (row instanceof Map) {
                entries = ((Map) row).entrySet();
            } else {
                entries = new LinkedHashSet<Map.Entry<String, Object>>();
                for (final String attrName : row.getAttributeNames()) {
                    entries.add(new Map.Entry<String, Object>() {
                        public String getKey() {
                            return attrName;
                        }

                        public Object getValue() {
                            return row.getAttribute(attrName);
                        }

                        public Object setValue(final Object value) {
                            final Object oldVal = getValue();
                            row.setAttribute(attrName, value);
                            return oldVal;
                        }
                    });
                }
            }
        }
        return entries;
    }

}
