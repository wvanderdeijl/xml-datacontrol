package org.adfemg.datacontrol.lov;

import oracle.adf.view.rich.model.QueryDescriptor;

public interface QueryModeHandler {
    void changeMode(QueryDescriptor.QueryMode mode);

    QueryDescriptor.QueryMode getMode();
}
