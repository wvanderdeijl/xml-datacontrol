package org.adfemg.datacontrol.lov;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCControlBinding;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.Criterion;

import oracle.binding.BindingContainer;

import oracle.jbo.ViewObject;


/**
 * Steps to use this:
 * <ol>
 *   <li>Create a table binding for the collection you want to use in the LOV
 *       in the page definition file of the page
 *     <ul>
 *       <li>Open the page in JDeveloper and navigate to the Bindings tab</li>
 *       <li>Click the green plus icon in the left column (Bindings) to add a
 *           binding</li>
 *       <li>Choose "table binding" as the binding type</li>
 *       <li>This will open a dialog to configure the new table binding
 *         <ul>
 *           <li>Choose the correct collection under the datacontrol that
 *               supplies the LOV data</li>
 *           <li>You can choose an iterator at the bottom of the dialog,
 *               but this should normally not exist yet</li>
 *           <li>Create a new iterator for that collection with the New...
 *               button and shuttle all attributes to "Display Attributes"</li>
 *         </ul>
 *       </li>
 *     </ul>
 *   </li>
 *   <li>set the RangeSize property of the new iterator in the pageDefinition
 *       from the default of 10 to -1 so all rows are retrieved.</li>
 *   <li>You'll need a bean that supplies the ListOfValues model. Create
 *       a subclass of this LovBean in your project. You can override methods
 *       in this bean if you want to customize the ListOfValues model.
 *       {@link #createModel} always needs to be overridden to set the mapping
 *       between LOV columns and attribute bindings. Other likely
 *       candidates are {@link #createTableModel} and
 *       {@link #createQueryDescriptor} but you could leave them alone if the
 *       default behavior is fine.</li>
 *   <li>Configure this new bean as viewScope bean in your taskflow and
 *       configure a single managed property named <code>controlBinding</code>
 *       and its value set to the name of the tableBinding you created earlier.</li>
 *   <li>Finally, you can add the inputListOfValues component in your page:<br/>
 *       <pre>&lt;af:inputListOfValues id="ilov1" label="country" popupTitle="Select a country"
 *                      value="#{viewScope.countryLovBean.lookup.description}"
 *                      model="#{viewScope.countryLovBean.lovModel}"
 *                      autoSubmit="true"/&gt;</pre>
 *     <ul>
 *       <li><code>value</code> points to the {@link #getLookup} method in
 *           this bean that returns a row from the LOV table binding. Within
 *           that row you can access its attributes ("description" in this
 *           example)</li>
 *       <li><code>model</code> points to the ListOfValuesModel supplied by
 *           the {@link #getLovModel} method in this bean</li>
 *       <li><code>autoSubmit</code> is enabled so the user can type (part of)
 *           the value and on exiting the field it will perform an auto-complete.
 *           The ListOfValuesModel will then check if exactly one row in the
 *           tableBinding matches. If so, it will select this row or otherwise
 *           it will open the LOV popup. Which column in the tableBinding is
 *           used for searching this autoComplete is determined by the
 *           QueryDescriptorImpl from {@link #createQueryDescriptor}. In the
 *           default implementation this will use the first column in the
 *           tableBinding</li>
 *     </ul>
 *   </li>
 * </ol>
 */
// TODO: document create methods based on examples in wiki as they contain
// very important info on how to set columnMappings and make other common
// customizations to the LOV's
public class LovBean {

    private static final ADFLogger logger = ADFLogger.createADFLogger(LovBean.class);

    private String controlBinding;

    private ModelImpl lovModel = null;

    public LovBean() {
        super();
    }

    protected ViewObject getViewObject() {
        if (controlBinding == null || controlBinding.isEmpty()) {
            throw new IllegalStateException("LovBean needs the Managed Property ControlBinding");
        }
        final BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        final DCControlBinding binding = (DCControlBinding) bindings.getControlBinding(controlBinding);
        if (binding == null) {
            throw new IllegalStateException("binding \"" + controlBinding + "\" not found");
        }
        return binding.getViewObject();
    }

    public ModelImpl getLovModel() {
        if (lovModel == null) { // Create LOVModel at first call.
            lovModel = createModel();
        }
        return lovModel;
    }

    public Map<String, Object> getLookup() {
        return getLovModel().getLookup();
    }

    protected ModelImpl createModel() {
        final ViewObject vo = getViewObject();
        logger.fine("creating ListOfValuesModel for {0}", vo);
        // TableModel for result-table
        final TableModelImpl resultTab = createTableModel(vo);
        // Build search criteria
        final ConjunctionCriterionImpl crits = createConjunction(resultTab);
        // QueryModel for saved searches and describe possible search-attributes
        final QueryModelImpl qmodel = createQueryModel(resultTab);
        // QueryDescriptor
        final QueryDescriptorImpl qdesc = createQueryDescriptor(qmodel, crits, vo);
        // the ListOfValuesModel
        return new ModelImpl(resultTab, qmodel, qdesc);
    }

    protected TableModelImpl createTableModel(final ViewObject vo) {
        return new TableModelImpl(vo);
    }

    protected ConjunctionCriterionImpl createConjunction(final TableModelImpl tableModel) {
        return new ConjunctionCriterionImpl(createCriteria(tableModel));
    }

    protected List<Criterion> createCriteria(final TableModelImpl tableModel) {
        final List<Criterion> retval = new ArrayList<Criterion>();
        for (AttributeDescriptor attr : tableModel.getAttributes()) {
            final Criterion crit = createCriterion(attr);
            if (crit != null) {
                retval.add(crit);
            }
        }
        return retval;
    }

    protected Criterion createCriterion(final AttributeDescriptor attr) {
        return new AttributeCriterionImpl(attr);
    }

    protected QueryModelImpl createQueryModel(final TableModelImpl tableModel) {
        return new QueryModelImpl(tableModel.getAttributes());
    }

    protected QueryDescriptorImpl createQueryDescriptor(final QueryModelImpl qmodel,
                                                        final ConjunctionCriterionImpl crits,
                                                        final ViewObject viewObject) {
        return new QueryDescriptorImpl(qmodel, crits, viewObject);
    }

    public void setControlBinding(final String controlBinding) {
        this.controlBinding = controlBinding;
    }

    public String getControlBinding() {
        return controlBinding;
    }
}
