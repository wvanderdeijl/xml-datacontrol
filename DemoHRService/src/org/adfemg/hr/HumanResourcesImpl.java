package org.adfemg.hr;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.math.BigInteger;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.adfemg.datacontrol.demo.Mapper;
import org.adfemg.datacontrol.demo.persist.Department;

@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
@WebService(name = "HumanResources", serviceName = "HumanResourcesService", targetNamespace = "http://adfemg.org/HR",
            portName = "HumanResourcesPort", wsdlLocation = "/HumanResourcesService.wsdl")
public class HumanResourcesImpl {

    private final ObjectFactory factory = new ObjectFactory();

    public HumanResourcesImpl() {
    }

    @WebResult(name = "ListAllDepartmentsResponse", partName = "parameters", targetNamespace = "http://adfemg.org/HR")
    @WebMethod(action = "listAllDepartments")
    public ListAllDepartmentsResponse listAllDepartments(@WebParam(name = "ListAllDepartmentsRequest",
                                                                   partName = "parameters",
                                                                   targetNamespace = "http://adfemg.org/HR")
                                                         ListAllDepartmentsRequest parameters) {
        ListAllDepartmentsResponse retval = factory.createListAllDepartmentsResponse();
        List<Department> depts = ofy().load().type(Department.class).list();
        retval.getDepartment().addAll(Mapper.deptsToSOA(depts));
        return retval;
    }

    @WebResult(name = "DepartmentEmployeesResponse", partName = "parameters", targetNamespace = "http://adfemg.org/HR")
    @WebMethod(action = "departmentEmployees")
    public DepartmentEmployeesResponse departmentEmployees(@WebParam(name = "DepartmentEmployeesRequest",
                                                                     partName = "parameters",
                                                                     targetNamespace = "http://adfemg.org/HR")
                                                           DepartmentEmployeesRequest parameters) {
        BigInteger deptId = parameters.getDepartmentId();
        DepartmentEmployeesResponse retval = factory.createDepartmentEmployeesResponse();
        if (deptId != null) {
            Department dept = ofy().load().type(Department.class).id(deptId.longValue()).now();
            if (dept != null) {
                retval.getEmployee().addAll(Mapper.empRefsToSOA(dept.getEmployees()));
            }
        }
        return retval;
    }
}
